import numpy as np

from SSE2 import M64, M128I, mm_setzero_si128, saturation, hadd, hsub


#### Intrinsics
def mm_abs_epi16(a: M128I) -> M128I:
    return M128I(np.absolute(a.get_i16()).view(np.uint16))

def mm_abs_epi32(a: M128I) -> M128I:
    return M128I(np.absolute(a.get_i32()).view(np.uint32))

def mm_abs_epi8(a: M128I) -> M128I:
    return M128I(np.absolute(a.get_i8()).view(np.uint8))

def mm_alignr_epi8(a: M128I, b: M128I, imm8: int) -> M128I:
    if 16 < imm8:
        raise NotImplementedError("imm8 greather than 16 not supported yet")
    tmp = np.concatenate((b.get_u8()[imm8:], a.get_u8()[:imm8]), dtype=np.uint8)
    return M128I(tmp)

def mm_hadd_epi16(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i16(), b.get_i16()), dtype=np.int16)
    return M128I(hadd(tmp))

def mm_hadd_epi32(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i32(), b.get_i32()), dtype=np.int32)
    return M128I(hadd(tmp))

def mm_hadds_epi16(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i16(), b.get_i16()), dtype=np.int32)
    return M128I(saturation(hadd(tmp), np.int16))

def mm_hsub_epi16(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i16(), b.get_i16()), dtype=np.int16)
    return M128I(hsub(tmp))

def mm_hsub_epi32(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i32(), b.get_i32()), dtype=np.int32)
    return M128I(hsub(tmp))

def mm_hsubs_epi16(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i16(), b.get_i16()), dtype=np.int32)
    return M128I(saturation(hsub(tmp), np.int16))

def mm_maddubs_epi16(a: M128I, b: M128I) -> M128I:
    mul = a.get_u8() * b.get_i8().astype(np.int32)
    return M128I(saturation(hadd(mul), np.int16))

def mm_mulhrs_epi16(a: M128I, b: M128I) -> M128I:
    tmp1 = a.get_i16() * b.get_i16().astype(np.int32)
    tmp2 = ((tmp1 >> 14) + 1) >> 1
    return M128I(tmp2.astype(np.int16))

def mm_shuffle_epi8(a: M128I, b: M128I) -> M128I:
    dst = mm_setzero_si128()
    dst_a = dst.get_u8()
    tmp = a.get_u8()
    for n, i in enumerate(b.get_u8()):
        if 128 <= i:
            dst_a[n] = 0
        else:
            dst_a[n] = tmp[i & 0xf]
    return dst

def mm_sign_epi16(a: M128I, b: M128I) -> M128I:
    tmp = a.get_i16().copy()
    for n, i in enumerate(b.get_i16()):
        if i < 0:
            tmp[n] = -tmp[n]
        else:
            tmp[n] = 0 if i == 0 else tmp[n]
    return M128I(tmp)

def mm_sign_epi32(a: M128I, b: M128I) -> M128I:
    tmp = a.get_i32().copy()
    for n, i in enumerate(b.get_i32()):
        if i < 0:
            tmp[n] = -tmp[n]
        else:
            tmp[n] = 0 if i == 0 else tmp[n]
    return M128I(tmp)

def mm_sign_epi8(a: M128I, b: M128I) -> M128I:
    tmp = a.get_i8().copy()
    for n, i in enumerate(b.get_i8()):
        if i < 0:
            tmp[n] = -tmp[n]
        else:
            tmp[n] = 0 if i == 0 else tmp[n]
    return M128I(tmp)

# =================== M64 ===================
# Not supported functions of the packed M64 type

def mm_abs_pi16(a: M64) -> M64:
    pass
def mm_abs_pi32(a: M64) -> M64:
    pass
def mm_abs_pi8(a: M64) -> M64:
    pass
def mm_alignr_pi8(a: M64, b: M64, imm8: int) -> M64:
    pass
def mm_hadd_pi16(a: M64, b: M64) -> M64:
    pass
def mm_hadd_pi32(a: M64, b: M64) -> M64:
    pass
def mm_hadds_pi16(a: M64, b: M64) -> M64:
    pass
def mm_hsub_pi16(a: M64, b: M64) -> M64:
    pass
def mm_hsub_pi32(a: M64, b: M64) -> M64:
    pass
def mm_hsubs_pi16(a: M64, b: M64) -> M64:
    pass
def mm_maddubs_pi16(a: M64, b: M64) -> M64:
    pass
def mm_mulhrs_pi16(a: M64, b: M64) -> M64:
    pass
def mm_shuffle_pi8(a: M64, b: M64) -> M64:
    pass
def mm_sign_pi16(a: M64, b: M64) -> M64:
    pass
def mm_sign_pi32(a: M64, b: M64) -> M64:
    pass
def mm_sign_pi8(a: M64, b: M64) -> M64:
    pass
