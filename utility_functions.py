import struct

import numpy as np


def addsub(a: np.ndarray, b: np.ndarray):
    tmp = a.copy()
    for n, i in enumerate(a):
        tmp[n] = a[n] - b[n] if n % 2 == 0 else a[n] + b[n]
    return tmp


def blend(a: np.ndarray, b: np.ndarray, control: int) -> np.ndarray:
    dst = np.empty(a.shape, dtype=a.dtype)
    for n, i in enumerate(zip(a, b)):
        ia, ib = i
        dst[n] = ib if (control & 1) else ia
        control = control >> 1
    return dst


def bool2floatmask(m):
    return np.uint32(bool2mask(m, 32)).view(np.float32)


def bool2mask(b, sz):
    return 2**sz-1 if b else 0


def bools_to_mask(bools, elem_size):
    elem = bool2mask(True, elem_size)
    o = [elem if b else 0 for b in bools]
    type_ = size2utype(elem_size)
    return np.array(o, dtype=type_)


# Horizontal add
# [a, a1, a2, a3, ... an-1, an] -> [a+a1, a2+a3, ... an-1+an]
# requires the length of `a` to be a multiple of two
def hadd(a: np.ndarray):
    length = a.shape[0] // 2
    array = np.empty((length, ), dtype=a.dtype)
    for n, _ in enumerate(array):
        array[n] = a[n*2] + a[n*2 + 1]
    return array


# Horizontal sub (see hadd)
def hsub(a: np.ndarray):
    length = a.shape[0] // 2
    array = np.empty((length, ), dtype=a.dtype)
    for n, _ in enumerate(array):
        array[n] = a[n*2] - a[n*2 + 1]
    return array


def insert_128(a256, b128, index):
    l1, l2 = a256.get_lanes()
    if index & 1:
        return [l1.copy(), b128.copy()]
    else:
        return [b128.copy(), l2.copy()]


def masked_load_pd(mem_addr: (bytes, int), mask: int):
    data, start = mem_addr
    dst = np.empty((2, ), dtype=np.float64)
    for n in range(2):
        if mask & 1:
            dst[n] = read_double((data, start+8*n))
        else:
            dst[n] = 0.0
        mask = mask >> 1
    return dst


def masked_load_ps(mem_addr: (bytes, int), mask: int):
    data, start = mem_addr
    dst = np.empty((4, ), dtype=np.float32)
    for n in range(4):
        if mask & 1:
            dst[n] = read_float((data, start+4*n))
        else:
            dst[n] = 0.0
        mask = mask >> 1
    return dst

def masked_load32(mem_addr: (bytes, int), mask: int):
    pass


def masked_load64(mem_addr: (bytes, int), mask: int):
    pass


def masked_store_pd(mem_addr: (bytes, int), a128, mask: int):
    data, start = mem_addr
    for n, i in enumerate(a128.get_doubles()):
        if mask & 1:
            write_doubles((data, start+8*n), [i])
        mask = mask >> 1
    return

def masked_store_ps(mem_addr: (bytes, int), a, mask: int):
    data, start = mem_addr
    for n, i in enumerate(a):
        if mask & 1:
            write_floats((data, start+8*n), [i])
        mask = mask >> 1
    return


def masked_store32(mem_addr: (bytes, int), a, mask: int):
    pass


def masked_store64(mem_addr: (bytes, int), a128, mask: int):
    pass


def permute256(a256, b256, control, null_func):
    tmp = a256.get_lanes()
    tmp.extend(b256.get_lanes())
    out = []
    for _ in range(2):
        if control & 8:
            out.append(null_func())
            continue

        i = control & 3
        control = control >> 4
        out.append(tmp[i].copy())
    return out


def permute4x64(a: np.ndarray, control: int):
    dst = np.empty((4, ), dtype=a.dtype)
    for i in range(4):
        dst[i] = a[control & 3]
        control = control >> 2
    return split_array(dst)


def permute8x32(a: np.ndarray, control: np.ndarray):
    dst = np.empty((8,), dtype=a.dtype)
    for n, i in enumerate(control.view(np.uint32)):
        dst[n] = a[i & 7]
    return split_array(dst)


def read_double(mem: (bytes, int)) -> float:
    data, start = mem
    return struct.unpack('d', bytes(data[start:start+8]))[0]


def read_float(mem: (bytes, int)) -> float:
    data, start = mem
    return struct.unpack('f', bytes(data[start:start+4]))[0]


def read_int(mem: (bytes, int), size_bits) -> int:
    data, start = mem
    sign = size_bits < 0
    size_bits = abs(size_bits)

    fmt = {8: 'b', 16: 'h', 32: 'i', 64: 'q'}[size_bits]
    if sign:
        fmt = fmt.upper()
    return struct.unpack(fmt, data[start:start + size_bits//8])[0]


def reverse_bits(num, sz):
    out = 0
    for i in range(sz):
        out = out << 1
        out |= num & 1
        num = num >> 1
    return out


def round_to(a, control):
    control = control & 0xf
    if control & 0x4:
        raise NotImplementedError("MXCSR.RC is not implemented.")
    if control == 3:
        return np.fix(a)  # -> to zero
    elif control == 2:
        return np.ceil(a)  # -> pos inf
    elif control == 1:
        return np.floor(a)  # -> neg inf
    else:  # control == 0
        return np.round(a)  # -> nearest


# clamps the values of `a` to the min/max value of `to_type` if they exceed the bounds
def saturation(a: np.ndarray, to_type):
    mx = np.iinfo(to_type).max
    mn = np.iinfo(to_type).min
    array = np.empty(a.shape, dtype=to_type)
    for n, i in enumerate(a):
        if mn <= i <= mx:
            array[n] = i
        else:
            array[n] = mn if i < 0 else mx
    return array


_NP_TYPES = {8: np.uint8, 16: np.uint16, 32: np.uint32, 64: np.uint64}
def size2utype(sz):
    return _NP_TYPES[sz]


# splits a array at the half-way point, `len(a)` has to be a even number
def split_array(a: np.ndarray):
    half = len(a) // 2
    l0 = a[:half]
    l1 = a[half:]
    return l0, l1


# Test the same as test_zf_cf but only considers the sign-bits
def test_signs(a: np.ndarray, b: np.ndarray) -> (int, int):  # (zf, cf)
    assert a.dtype == b.dtype
    assert len(a) == len(b)
    bit = (np.iinfo(a.dtype).max + 1) // 1
    zf = True
    cf = True
    for i, j in zip(a, b):
        zf &= (i & j & bit) == 0
        cf &= (~i & j & bit) == 0
    return int(zf), int(cf)


# Sets `zf` to true if (a & b == 0) and sets `cf` if (~a & b) == b
def test_zf_cf(a: np.ndarray, b: np.ndarray) -> (int, int):  # (zf, cf)
    zf = True
    cf = True
    for i, j in zip(a.view(np.uint32), b.view(np.uint32)):
        zf &= (i & j) == 0
        cf &= (~i & j) == 0
    return int(zf), int(cf)


def write_doubles(mem: (bytes, int), vals: [float]) -> None:
    data, start = mem
    i = 0
    for f in vals:
        for b in struct.pack('d', f):
            data[start + i] = b
            i += 1
    return

def write_floats(mem: (bytes, int), vals: [float]) -> None:
    data, start = mem
    i = 0
    for f in vals:
        for b in struct.pack('f', f):
            data[start + i] = b
            i += 1
    return

def write_int(mem: (bytes, int), vals: np.ndarray) -> None:
    data, start = mem
    a = vals.view(np.int8)
    for n, i in enumerate(a):
        data[start + n] = i
    return


# `m`s elements need to be unsigned
def vector2mask(m: np.ndarray) -> int:
    control = 0
    bit = (np.iinfo(m[0]).max + 1) // 2
    for i in reversed(m):
        control = control << 1
        if i & bit:
            control += 1
    return control


def vshift_left(a: np.ndarray, control: np.ndarray):
    return _vshift(a, control, True)

def vshift_right(a: np.ndarray, control: np.ndarray):
    return _vshift(a, control, False)

def _vshift(a: np.ndarray, control: np.ndarray, left):
    dst = a.copy()
    for n, z in enumerate(zip(a, control)):
        i, shift = z
        if left:
            dst[n] = i << shift
        else:
            dst[n] = i >> shift
    return dst
