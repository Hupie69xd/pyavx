import SSE
import SSE2
import SSE3
import SSSE3
import SSE4_1
#import SSE4_2
import AVX

from AVX import M256, M256D, M256I, use_sse_def
from SSE import M128
from SSE2 import M128D, M128I
from utility_functions import *

import numpy as np


# mini helper
def convert256(elements: np.ndarray):
    l0, l1 = split_array(elements)
    return M256I([M128I(l0), M128I(l1)])

def concat_lanes(a256):
    l0, l1 = a256.get_lanes()
    if isinstance(l0, M128I):
        l0 = l0.get_i32()
        l1 = l1.get_i32()
    elif isinstance(l0, M128D):
        l0 = l0.get_doubles()
        l1 = l1.get_doubles()
    elif isinstance(l0, M128):
        l0 = l0.get_floats()
        l1 = l1.get_floats()
    else:
        raise TypeError("a256's lanes are not of type M128, M128D, or M128I")
    return np.concatenate((l0, l1))


### Intrinsics
def mm256_abs_epi16(a: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_abs_epi16, a))

def mm256_abs_epi32(a: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_abs_epi32, a))

def mm256_abs_epi8(a: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_abs_epi8, a))

def mm256_add_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_add_epi16, a, b))

def mm256_add_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_add_epi32, a, b))

def mm256_add_epi64(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_add_epi64, a, b))

def mm256_add_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_add_epi8, a, b))

def mm256_adds_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_adds_epi16, a, b))

def mm256_adds_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_adds_epi8, a, b))

def mm256_adds_epu16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_adds_epu16, a, b))

def mm256_adds_epu8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_adds_epu8, a, b))

def mm256_alignr_epi8(a: M256I, b: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_alignr_epi8, a, b, imm8))

def mm256_and_si256(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_and_si128, a, b))

def mm256_andnot_si256(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_andnot_si128, a, b))

def mm256_avg_epu16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_avg_epu16, a, b))

def mm256_avg_epu8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_avg_epu8, a, b))

def mm256_blend_epi16(a: M256I, b: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_blend_epi16, a, b, imm8))

def mm_blend_epi32(a: M128I, b: M128I, imm8: int) -> M128I:
    return M128I(blend(a.get_i32(), b.get_i32(), imm8))

def mm256_blend_epi32(a: M256I, b: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(mm_blend_epi32, a, b, imm8))

def mm256_blendv_epi8(a: M256I, b: M256I, mask: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_blendv_epi8, a, b, mask))

def mm_broadcastb_epi8(a: M128I) -> M128I:
    return SSE2.mm_set1_epi8(a.get_i8()[0])

def mm256_broadcastb_epi8(a: M128I) -> M256I:
    return AVX.mm256_set1_epi8(a.get_i8()[0])

def mm_broadcastd_epi32(a: M128I) -> M128I:
    return SSE2.mm_set1_epi32(a.get_i32()[0])

def mm256_broadcastd_epi32(a: M128I) -> M256I:
    return AVX.mm256_set1_epi32(a.get_i32()[0])

def mm_broadcastq_epi64(a: M128I) -> M128I:
    return SSE2.mm_set1_epi64x(a.get_i64()[0])

def mm256_broadcastq_epi64(a: M128I) -> M256I:
    return AVX.mm256_set1_epi64x(a.get_i64()[0])

def mm_broadcastsd_pd(a: M128D) -> M128D:
    return SSE2.mm_set1_pd(a.get_first())

def mm256_broadcastsd_pd(a: M128D) -> M256D:
    return AVX.mm256_set1_pd(a.get_first())

def mm_broadcastsi128_si256(a: M128I) -> M256I:
    return M256I([a.copy(), a.copy()])

def mm256_broadcastsi128_si256(a: M128I) -> M256I:
    return mm_broadcastsi128_si256(a)

def mm_broadcastss_ps(a: M128) -> M128:
    return SSE.mm_set1_ps(a.get_first())

def mm256_broadcastss_ps(a: M128) -> M256:
    return AVX.mm256_set1_ps(a.get_first())

def mm_broadcastw_epi16(a: M128I) -> M128I:
    return SSE2.mm_set1_epi16(a.get_i16()[0])

def mm256_broadcastw_epi16(a: M128I) -> M256I:
    return AVX.mm256_set1_epi16(a.get_i16()[0])

def mm256_bslli_epi128(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_bslli_si128, a, imm8))

def mm256_bsrli_epi128(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_bsrli_si128, a, imm8))

def mm256_cmpeq_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_cmpeq_epi16, a, b))

def mm256_cmpeq_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_cmpeq_epi32, a, b))

def mm256_cmpeq_epi64(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_cmpeq_epi64, a, b))

def mm256_cmpeq_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_cmpeq_epi8, a, b))

def mm256_cmpgt_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_cmpgt_epi16, a, b))

def mm256_cmpgt_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_cmpgt_epi32, a, b))

def mm256_cmpgt_epi64(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_2.mm_cmpgt_epi64, a, b))

def mm256_cmpgt_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_cmpgt_epi8, a, b))

def mm256_cvtepi16_epi32(a: M128I) -> M256I:
    return convert256(a.get_i16().astype(np.int32))

def mm256_cvtepi16_epi64(a: M128I) -> M256I:
    return convert256(a.get_i16()[:4].astype(np.int64))

def mm256_cvtepi32_epi64(a: M128I) -> M256I:
    return convert256(a.get_i32().astype(np.int64))

def mm256_cvtepi8_epi16(a: M128I) -> M256I:
    return convert256(a.get_i8().astype(np.int16))

def mm256_cvtepi8_epi32(a: M128I) -> M256I:
    return convert256(a.get_i8()[:8].astype(np.int32))

def mm256_cvtepi8_epi64(a: M128I) -> M256I:
    return convert256(a.get_i8()[:4].astype(np.int64))

def mm256_cvtepu16_epi32(a: M128I) -> M256I:
    return convert256(a.get_u16().astype(np.uint32))

def mm256_cvtepu16_epi64(a: M128I) -> M256I:
    return convert256(a.get_u16()[:4].astype(np.uint64))

def mm256_cvtepu32_epi64(a: M128I) -> M256I:
    return convert256(a.get_u32().astype(np.uint64))

def mm256_cvtepu8_epi16(a: M128I) -> M256I:
    return convert256(a.get_u8().astype(np.uint16))

def mm256_cvtepu8_epi32(a: M128I) -> M256I:
    return convert256(a.get_u8()[:8].astype(np.uint32))

def mm256_cvtepu8_epi64(a: M128I) -> M256I:
    return convert256(a.get_u8()[:4].astype(np.uint64))

def mm256_extract_epi16(a: M256I, index: int) -> int:
    index = index & 0xf
    lane, i = divmod(index, 8)
    return a.get_lanes()[lane][i]

def mm256_extract_epi8(a: M256I, index: int) -> int:
    index = index & 0x1f
    lane, i = divmod(index, 16)
    return a.get_lanes()[lane][i]

def mm256_extracti128_si256(a: M256I, imm8: int) -> M128I:
    return a.get_lanes()[imm8 & 0]

def mm256_hadd_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_hadd_epi16, a, b))

def mm256_hadd_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_hadd_epi32, a, b))

def mm256_hadds_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_hadds_epi16, a, b))

def mm256_hsub_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_hsub_epi16, a, b))

def mm256_hsub_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_hsub_epi32, a, b))

def mm256_hsubs_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_hsubs_epi16, a, b))

##def mm_i32gather_epi32(base_addr: int*, vindex: M128I, scale: int) -> M128I:
##def mm_mask_i32gather_epi32(src: M128I, base_addr: int*, vindex: M128I, mask: M128I, scale: int) -> M128I:
##def mm256_i32gather_epi32(base_addr: int*, vindex: M256I, scale: int) -> M256I:
##def mm256_mask_i32gather_epi32(src: M256I, base_addr: int*, vindex: M256I, mask: M256I, scale: int) -> M256I:
##def mm_i32gather_epi64(base_addr: Int64*, vindex: M128I, scale: int) -> M128I:
##def mm_mask_i32gather_epi64(src: M128I, base_addr: Int64*, vindex: M128I, mask: M128I, scale: int) -> M128I:
##def mm256_i32gather_epi64(base_addr: Int64*, vindex: M128I, scale: int) -> M256I:
##def mm256_mask_i32gather_epi64(src: M256I, base_addr: Int64*, vindex: M128I, mask: M256I, scale: int) -> M256I:
##def mm_i32gather_pd(base_addr: double*, vindex: M128I, scale: int) -> M128D:
##def mm_mask_i32gather_pd(src: M128D, base_addr: double*, vindex: M128I, mask: M128D, scale: int) -> M128D:
##def mm256_i32gather_pd(base_addr: double*, vindex: M128I, scale: int) -> M256D:
##def mm256_mask_i32gather_pd(src: M256D, base_addr: double*, vindex: M128I, mask: M256D, scale: int) -> M256D:
##def mm_i32gather_ps(base_addr: float*, vindex: M128I, scale: int) -> M128:
##def mm_mask_i32gather_ps(src: M128, base_addr: float*, vindex: M128I, mask: M128, scale: int) -> M128:
##def mm256_i32gather_ps(base_addr: float*, vindex: M256I, scale: int) -> M256:
##def mm256_mask_i32gather_ps(src: M256, base_addr: float*, vindex: M256I, mask: M256, scale: int) -> M256:
##def mm_i64gather_epi32(base_addr: int*, vindex: M128I, scale: int) -> M128I:
##def mm_mask_i64gather_epi32(src: M128I, base_addr: int*, vindex: M128I, mask: M128I, scale: int) -> M128I:
##def mm256_i64gather_epi32(base_addr: int*, vindex: M256I, scale: int) -> M128I:
##def mm256_mask_i64gather_epi32(src: M128I, base_addr: int*, vindex: M256I, mask: M128I, scale: int) -> M128I:
##def mm_i64gather_epi64(base_addr: Int64*, vindex: M128I, scale: int) -> M128I:
##def mm_mask_i64gather_epi64(src: M128I, base_addr: Int64*, vindex: M128I, mask: M128I, scale: int) -> M128I:
##def mm256_i64gather_epi64(base_addr: Int64*, vindex: M256I, scale: int) -> M256I:
##def mm256_mask_i64gather_epi64(src: M256I, base_addr: Int64*, vindex: M256I, mask: M256I, scale: int) -> M256I:
##def mm_i64gather_pd(base_addr: double*, vindex: M128I, scale: int) -> M128D:
##def mm_mask_i64gather_pd(src: M128D, base_addr: double*, vindex: M128I, mask: M128D, scale: int) -> M128D:
##def mm256_i64gather_pd(base_addr: double*, vindex: M256I, scale: int) -> M256D:
##def mm256_mask_i64gather_pd(src: M256D, base_addr: double*, vindex: M256I, mask: M256D, scale: int) -> M256D:
##def mm_i64gather_ps(base_addr: float*, vindex: M128I, scale: int) -> M128:
##def mm_mask_i64gather_ps(src: M128, base_addr: float*, vindex: M128I, mask: M128, scale: int) -> M128:
##def mm256_i64gather_ps(base_addr: float*, vindex: M256I, scale: int) -> M128:
##def mm256_mask_i64gather_ps(src: M128, base_addr: float*, vindex: M256I, mask: M128, scale: int) -> M128:
def mm256_inserti128_si256(a: M256I, b: M128I, imm8: int) -> M256I:
    return M256I(AVX.insert_128(a, b, imm8))

def mm256_madd_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_madd_epi16, a, b))

def mm256_maddubs_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_maddubs_epi16, a, b))

def mm_maskload_epi32(mem_addr: (bytes, int), mask: M128I) -> M128I:
    i_mask = vector2mask(mask.get_u32())
    return M128I(masked_load32(mem_addr, i_mask))

def mm256_maskload_epi32(mem_addr: (bytes, int), mask: M256I) -> M256I:
    data, start = mem_addr
    m0, m1 = mask.get_lanes()
    l0 = mm_maskload_epi32(mem_addr, m0)
    l1 = mm_maskload_epi32((data, start + 16), m1)
    return AVX.mm256_set_m128i(l1, l0)

def mm_maskload_epi64(mem_addr: (bytes, int), mask: M128I) -> M128I:
    i_mask = vector2mask(mask.get_u64())
    return M128I(masked_load64(mem_addr, i_mask))

def mm256_maskload_epi64(mem_addr: (bytes, int), mask: M256I) -> M256I:
    data, start = mem_addr
    m0, m1 = mask.get_lanes()
    l0 = mm_maskload_epi64(mem_addr, m0)
    l1 = mm_maskload_epi64((data, start + 16), m1)
    return AVX.mm256_set_m128i(l1, l0)

def mm_maskstore_epi32(mem_addr: (bytes, int), mask: M128I, a: M128I) -> None:
    i_mask = vector2mask(mask.get_u32())
    masked_store32(mem_addr, a.get_i32, i_mask)

def mm256_maskstore_epi32(mem_addr: (bytes, int), mask: M256I, a: M256I) -> None:
    data, start = mem_addr
    l0, l1 = a.get_lanes()
    m0, m1 = mask.get_lanes()
    mm_maskstore_epi32(mem_addr, m0, l0)
    mm_maskstore_epi32((data, start + 16), m1, l1)

def mm_maskstore_epi64(mem_addr: (bytes, int), mask: M128I, a: M128I) -> None:
    i_mask = vector2mask(mask.get_u64())
    masked_store64(mem_addr, a.get_i64, i_mask)

def mm256_maskstore_epi64(mem_addr: (bytes, int), mask: M256I, a: M256I) -> None:
    data, start = mem_addr
    l0, l1 = a.get_lanes()
    m0, m1 = mask.get_lanes()
    mm_maskstore_epi64(mem_addr, m0, l0)
    mm_maskstore_epi64((data, start + 16), m1, l1)

def mm256_max_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_max_epi16, a, b))

def mm256_max_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_max_epi32, a, b))

def mm256_max_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_max_epi8, a, b))

def mm256_max_epu16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_max_epu16, a, b))

def mm256_max_epu32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_max_epu32, a, b))

def mm256_max_epu8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_max_epu8, a, b))

def mm256_min_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_min_epi16, a, b))

def mm256_min_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_min_epi32, a, b))

def mm256_min_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_min_epi8, a, b))

def mm256_min_epu16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_min_epu16, a, b))

def mm256_min_epu32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_min_epu32, a, b))

def mm256_min_epu8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_min_epu8, a, b))

def mm256_movemask_epi8(a: M256I) -> int:
    return int(use_sse_def(SSE2.mm_movemask_epi8, a))

def mm256_mpsadbw_epu8(a: M256I, b: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_mpsadbw_epu8, a, b, imm8))

def mm256_mul_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_mul_epi32, a, b))

def mm256_mul_epu32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_mul_epu32, a, b))

def mm256_mulhi_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_mulhi_epi16, a, b))

def mm256_mulhi_epu16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_mulhi_epu16, a, b))

def mm256_mulhrs_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_mulhrs_epi16, a, b))

def mm256_mullo_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_mullo_epi16, a, b))

def mm256_mullo_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_mullo_epi32, a, b))

def mm256_or_si256(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_or_si128, a, b))

def mm256_packs_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_packs_epi16, a, b))

def mm256_packs_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_packs_epi32, a, b))

def mm256_packus_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_packus_epi16, a, b))

def mm256_packus_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE4_1.mm_packus_epi32, a, b))

def mm256_permute2x128_si256(a: M256I, b: M256I, imm8: int) -> M256I:
    return AVX.mm256_permute2f128_si256(a, b, imm8)

def mm256_permute4x64_epi64(a: M256I, imm8: int) -> M256I:
    tmp = concat_lanes(a)
    o0, o1 = permute4x64(tmp, imm8)
    return AVX.mm256_set_m128i(o1, o0)

def mm256_permute4x64_pd(a: M256D, imm8: int) -> M256D:
    tmp = concat_lanes(a)
    o0, o1 = permute4x64(tmp, imm8)
    return AVX.mm256_set_m128d(o1, o0)

def mm256_permutevar8x32_epi32(a: M256I, idx: M256I) -> M256I:
    tmp1 = concat_lanes(a)
    tmp2 = concat_lanes(idx)
    o0, o1 = permute8x32(tmp1, tmp2)
    return AVX.mm256_set_m128i(o1, o0)

def mm256_permutevar8x32_ps(a: M256, idx: M256I) -> M256:
    tmp1 = concat_lanes(a)
    tmp2 = concat_lanes(idx)
    o0, o1 = permute8x32(tmp1, tmp2)
    return AVX.mm256_set_m128(o1, o0)

def mm256_sad_epu8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sad_epu8, a, b))

def mm256_shuffle_epi32(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_shuffle_epi32, a, imm8))

def mm256_shuffle_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_shuffle_epi8, a, b))

def mm256_shufflehi_epi16(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_shufflehi_epi16, a, imm8))

def mm256_shufflelo_epi16(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_shufflelo_epi16, a, imm8))

def mm256_sign_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_sign_epi16, a, b))

def mm256_sign_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_sign_epi32, a, b))

def mm256_sign_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSSE3.mm_sign_epi8, a, b))

def mm256_sll_epi16(a: M256I, count: M128I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sll_epi16, a, count))

def mm256_sll_epi32(a: M256I, count: M128I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sll_epi32, a, count))

def mm256_sll_epi64(a: M256I, count: M128I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sll_epi64, a, count))

def mm256_slli_epi16(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_slli_epi16, a, imm8))

def mm256_slli_epi32(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_slli_epi32, a, imm8))

def mm256_slli_epi64(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_slli_epi64, a, imm8))

def mm256_slli_si256(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_slli_si128, a, imm8))

def mm_sllv_epi32(a: M128I, count: M128I) -> M128I:
    return M128I(vshift_left(a.get_u32(), count.get_u32()))

def mm256_sllv_epi32(a: M256I, count: M256I) -> M256I:
    return M256I(use_sse_def(mm_sllv_epi32, a, count))

def mm_sllv_epi64(a: M128I, count: M128I) -> M128I:
    return M128I(vshift_left(a.get_u64(), count.get_u64()))

def mm256_sllv_epi64(a: M256I, count: M256I) -> M256I:
    return M256I(use_sse_def(mm_sllv_epi64, a, count))

def mm256_sra_epi16(a: M256I, count: M128I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sra_epi16, a, count))

def mm256_sra_epi32(a: M256I, count: M128I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sra_epi32, a, count))

def mm256_srai_epi16(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srai_epi16, a, imm8))

def mm256_srai_epi32(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srai_epi32, a, imm8))

def mm_srav_epi32(a: M128I, count: M128I) -> M128I:
    return M128I(vshift_right(a.get_i32(), count.get_u32()))

def mm256_srav_epi32(a: M256I, count: M256I) -> M256I:
    return M256I(use_sse_def(mm_srav_epi32, a, count))

def mm256_srl_epi16(a: M256I, count: M128I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srl_epi16, a, count))

def mm256_srl_epi32(a: M256I, count: M128I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srl_epi32, a, count))

def mm256_srl_epi64(a: M256I, count: M128I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srl_epi64, a, count))

def mm256_srli_epi16(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srli_epi16, a, imm8))

def mm256_srli_epi32(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srli_epi32, a, imm8))

def mm256_srli_epi64(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srli_epi64, a, imm8))

def mm256_srli_si256(a: M256I, imm8: int) -> M256I:
    return M256I(use_sse_def(SSE2.mm_srli_si128, a, imm8))

def mm_srlv_epi32(a: M128I, count: M128I) -> M128I:
    return M128I(vshift_right(a.get_u32(), count.get_u32()))

def mm256_srlv_epi32(a: M256I, count: M256I) -> M256I:
    return M256I(use_sse_def(mm_srlv_epi32, a, count))

def mm_srlv_epi64(a: M128I, count: M128I) -> M128I:
    return M128I(vshift_right(a.get_u64(), count.get_u64()))

def mm256_srlv_epi64(a: M256I, count: M256I) -> M256I:
    return M256I(use_sse_def(mm_srlv_epi64, a, count))

def mm256_stream_load_si256(mem_addr: (bytes, int)) -> M256I:
    return AVX.mm256_load_si256(mem_addr)

def mm256_sub_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sub_epi16, a, b))

def mm256_sub_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sub_epi32, a, b))

def mm256_sub_epi64(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sub_epi64, a, b))

def mm256_sub_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_sub_epi8, a, b))

def mm256_subs_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_subs_epi16, a, b))

def mm256_subs_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_subs_epi8, a, b))

def mm256_subs_epu16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_subs_epu16, a, b))

def mm256_subs_epu8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_subs_epu8, a, b))

def mm256_unpackhi_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_unpackhi_epi16, a, b))

def mm256_unpackhi_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_unpackhi_epi32, a, b))

def mm256_unpackhi_epi64(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_unpackhi_epi64, a, b))

def mm256_unpackhi_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_unpackhi_epi8, a, b))

def mm256_unpacklo_epi16(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_unpacklo_epi16, a, b))

def mm256_unpacklo_epi32(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_unpacklo_epi32, a, b))

def mm256_unpacklo_epi64(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_unpacklo_epi64, a, b))

def mm256_unpacklo_epi8(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_unpacklo_epi8, a, b))

def mm256_xor_si256(a: M256I, b: M256I) -> M256I:
    return M256I(use_sse_def(SSE2.mm_xor_si128, a, b))
