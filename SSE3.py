import numpy as np

from SSE import M128
from SSE2 import M128D, M128I, mm_load_si128, mm_load_pd1, mm_set1_pd
from utility_functions import addsub, hadd, hsub


#### Intrinsics
def mm_addsub_pd(a: M128D, b: M128D) -> M128D:
    return M128D(addsub(a.get_doubles(), b.get_doubles()))

def mm_addsub_ps(a: M128, b: M128) -> M128:
    return M128(addsub(a.get_floats(), b.get_floats()))

def mm_hadd_pd(a: M128D, b: M128D) -> M128D:
    tmp = np.concatenate((a.get_doubles(), b.get_doubles()), dtype=np.float64)
    return M128D(hadd(tmp))

def mm_hadd_ps(a: M128, b: M128) -> M128:
    tmp = np.concatenate((a.get_floats(), b.get_floats()), dtype=np.float32)
    return M128(hadd(tmp))

def mm_hsub_pd(a: M128D, b: M128D) -> M128D:
    tmp = np.concatenate((a.get_doubles(), b.get_doubles()), dtype=np.float64)
    return M128D(hsub(tmp))

def mm_hsub_ps(a: M128, b: M128) -> M128:
    tmp = np.concatenate((a.get_floats(), b.get_floats()), dtype=np.float32)
    return M128(hsub(tmp))

def mm_lddqu_si128(mem_addr: (bytes, int)) -> M128I:
    return mm_load_si128(mem_addr)

def mm_loaddup_pd(mem_addr: (bytes, int)) -> M128D:
    return mm_load_pd1(mem_addr)

def mm_movedup_pd(a: M128D) -> M128D:
    return mm_set1_pd(a.get_first())

def mm_movehdup_ps(a: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = tmp[1]
    tmp[2] = tmp[3]
    return M128(tmp)

def mm_moveldup_ps(a: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[1] = tmp[0]
    tmp[3] = tmp[2]
    return M128(tmp)
