from __future__ import annotations

import numpy as np

from utility_functions import *

class Lane:
    pass


def extract_funcs(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        data = f.read()
    data = data.split('\n')
    filt = filter(lambda l: '(' in l, data)
    return list(filt)
def write_funcs(filename: str, funcs: [str]):
    with open(filename, 'w', encoding='utf-8') as f:
        f.write('\n'.join(funcs))


def type_format(args):
    if args == [''] or args == ["void"]:
        return ''

    output = []
    for i in args:
        t, name = i.strip().rsplit(' ', maxsplit=1)
        if t.startswith("__"):
            t = t[2:].title()
        output.append(f"{name}: {t}")
    return ", ".join(output)

def decompose_func(func):
    ret, remain = func.split(' ', 1)
    p = remain.find('(')
    name = remain[:p]
    args = remain[p+1:-1].split(', ')
    return ret.strip(), name.strip(), args

_TYPE_CONVERTIONS = {"int": "int64_t", "__int64": "int64_t", "unsigned int": "int64_t",
                     "short": "int64_t", "char": "int64_t", "unsigned": "int64_t",
                     "float": "double"}
def hammer_args(args):
    args = [i.rsplit(' ', 1)[0] for i in args]
    out = []
    for i in args:
        c = _TYPE_CONVERTIONS.get(i)
        out.append(c if c is not None else i)
    return out

def split_into_cats(funcs):
    cats = {}
    for i in funcs:
        ret, name, args = decompose_func(i)
        args = hammer_args(args)
        ret = hammer_args([ret])[0]
        if args == ['']:
            args = ["void"]
        sig = ret + " <- " + ','.join(args)
        if cats.get(sig) is None:
            cats[sig] = []
        cats[sig].append(name)
    return cats


_TYPE_TABLE = {"void": "None", "int": "int", "unsigned int": "int", "void*": "ERROR",
               "float": "float", "double": "float", "char": 'int', "short": "int",
               "unsigned __int64": 'int'}

def convert_func(func):
    ret, name, args = decompose_func(func)
    if ret.startswith("__"):
        ret_type = ret[2:].title()
    else:
        ret_type = _TYPE_TABLE[ret.strip()]

    func_name = name[1:]
    args = type_format(args)
    return f"def {func_name}({args}) -> {ret_type}:"

##write_funcs("SSE2_funcs.txt", extract_funcs("SSE2.txt"))
##write_funcs("SSE2_py.txt", [convert_func(i) for i in extract_funcs("SSE2.txt")])

class M64(Lane):
    pass

class M128(Lane):
    def __init__(self, elements: np.ndarray):
        assert len(elements) == 4, "Need 4 floats"
        assert elements.dtype == np.float32, "Invalid dtype is " + str(elements.dtype)
        self._ele = elements
    def as_uints(self, size=64) -> np.ndarray:
        return self._ele.view(size2utype(size))
    def copy(self) -> M128:
        return M128(self._ele.copy())
    def get_first(self) -> np.float32:
        return self._ele[0]
    def get_floats(self) -> np.ndarray:
        return self._ele
    def set_first(self, value) -> None:
        self._ele[0] = value


#### SSE Intrinsics
def mm_add_ps(a: M128, b: M128) -> M128:
    return M128(a.get_floats() + b.get_floats())

def mm_add_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(a.get_first() + b.get_first())
    return dst

def mm_and_ps(a: M128, b: M128) -> M128:
    return M128((a.as_uints() & b.as_uints()).view(np.float32))

def mm_andnot_ps(a: M128, b: M128) -> M128:
    return M128(((~a.as_uints()) & b.as_uints()).view(np.float32))

def mm_cmpeq_ps(a: M128, b: M128) -> M128:
    return M128(bools_to_mask(a.get_floats() == b.get_floats(), 32).view(np.float32))

def mm_cmpeq_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() == b.get_first()))
    return dst

def mm_cmpge_ps(a: M128, b: M128) -> M128:
    return M128(bools_to_mask(a.get_floats() >= b.get_floats(), 32).view(np.float32))

def mm_cmpge_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() >= b.get_first()))
    return dst

def mm_cmpgt_ps(a: M128, b: M128) -> M128:
    return M128(bools_to_mask(a.get_floats() > b.get_floats(), 32).view(np.float32))

def mm_cmpgt_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() > b.get_first()))
    return dst

def mm_cmple_ps(a: M128, b: M128) -> M128:
    return mm_cmpge_ps(b, a)

def mm_cmple_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() <= b.get_first()))
    return dst

def mm_cmplt_ps(a: M128, b: M128) -> M128:
    return mm_cmpgt_ps(b, a)

def mm_cmplt_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() < b.get_first()))
    return dst

def mm_cmpneq_ps(a: M128, b: M128) -> M128:
    return M128(bools_to_mask(a.get_floats() != b.get_floats(), 32).view(np.float32))

def mm_cmpneq_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() != b.get_first()))
    return dst

def mm_cmpnge_ps(a: M128, b: M128) -> M128:
    return mm_cmplt_ps(a, b)

def mm_cmpnge_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() < b.get_first()))
    return dst

def mm_cmpngt_ps(a: M128, b: M128) -> M128:
    return mm_cmple_ps(a, b)

def mm_cmpngt_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() <= b.get_first()))
    return dst

def mm_cmpnle_ps(a: M128, b: M128) -> M128:
    return mm_cmpgt_ps(a, b)

def mm_cmpnle_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(a.get_first() > b.get_first()))
    return dst

def mm_cmpnlt_ps(a: M128, b: M128) -> M128:
    return mm_cmpge_ps(a, b)

def mm_cmpnlt_ss(a: M128, b: M128) -> M128:
    return mm_cmpge_ss(a, b)

def mm_cmpord_ps(a: M128, b: M128) -> M128:
    return M128(bools_to_mask(~ np.isnan(a.get_floats()) &
                              ~ np.isnan(b.get_floats()), 32).view(np.float32))

def mm_cmpord_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    b = np.isnan(a.get_first()) or np.isnan(b.get_first())
    dst.set_first(bool2floatmask(not b))
    return dst

def mm_cmpunord_ps(a: M128, b: M128) -> M128:
    return M128(bools_to_mask(np.isnan(a.get_floats()) | np.isnan(b.get_floats()), 32).view(np.float32))

def mm_cmpunord_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(bool2floatmask(np.isnan(a.get_first()) | np.isnan(b.get_first())))
    return dst

def mm_comieq_ss(a: M128, b: M128) -> int:
    return int(a.get_first() == b.get_first())

def mm_comige_ss(a: M128, b: M128) -> int:
    return int(a.get_first() >= b.get_first())

def mm_comigt_ss(a: M128, b: M128) -> int:
    return int(a.get_first() > b.get_first())

def mm_comile_ss(a: M128, b: M128) -> int:
    return mm_comige_ss(b, a)

def mm_comilt_ss(a: M128, b: M128) -> int:
    return mm_comigt_ss(b, a)

def mm_comineq_ss(a: M128, b: M128) -> int:
    return 1 - mm_comieq_ss(a, b)

def mm_cvt_si2ss(a: M128, b: int) -> M128:
    dst = a.copy()
    dst.set_first(np.float32(b))
    return dst

def mm_cvt_ss2si(a: M128) -> int:
    return int(a.get_first())

def mm_cvtsi32_ss(a: M128, b: int) -> M128:
    dst = a.copy()
    dst.set_first(np.float32(b))
    return dst

def mm_cvtsi64_ss(a: M128, b: int) -> M128:
    return mm_cvtsi32_ss(a, b)

def mm_cvtss_f32(a: M128) -> float:
    return a.get_first()

def mm_cvtss_si32(a: M128) -> int:
    return np.int32(a.get_first())

def mm_cvtss_si64(a: M128) -> int:
    return np.int64(a.get_first())

def mm_cvtt_ss2si(a: M128) -> int:
    return np.int32(a.get_first())

def mm_cvttss_si32(a: M128) -> int:
    return mm_cvtt_ss2si(a)

def mm_cvttss_si64(a: M128) -> int:
    return np.int64(a.get_first())

def mm_div_ps(a: M128, b: M128) -> M128:
    return M128(a.get_floats() / b.get_floats())

def mm_div_ss(a: M128, b: M128) -> M128:
    dst = a.copy()
    dst.set_first(a.get_first() / b.get_first())
    return dst

def mm_free(_) -> None:
    raise NotImplementedError("Function not implemented.")

def MM_GET_EXCEPTION_MASK () -> int:
    raise NotImplementedError("Function not implemented.")

def MM_GET_EXCEPTION_STATE () -> int:
    raise NotImplementedError("Function not implemented.")

def MM_GET_FLUSH_ZERO_MODE () -> int:
    raise NotImplementedError("Function not implemented.")

def MM_GET_ROUNDING_MODE () -> int:
    raise NotImplementedError("Function not implemented.")

def mm_getcsr () -> int:
    raise NotImplementedError("Function not implemented.")

def mm_load_ps(mem_addr: (bytes, int)) -> M128:
    data, start = mem_addr
    elem = [read_float((data, start + n * 4)) for n in range(4)]
    return mm_setr_ps(*elem)

def mm_load_ps1(mem_addr: (bytes, int)) -> M128:
    return mm_set_ps1(read_float(mem_addr))

def mm_load_ss(mem_addr: (bytes, int)) -> M128:
    return mm_set_ss(read_float(mem_addr))

def mm_load1_ps(mem_addr: (bytes, int)) -> M128:
    return mm_load_ps1(mem_addr)

def mm_loadh_pi(a: M128, mem_addr: (bytes, int)) -> M128:
    val1 = read_float(mem_addr)
    data, start = mem_addr
    val2 = read_float((data, start+4))
    tmp = a.get_floats().copy()
    tmp[2] = val1
    tmp[3] = val2
    return M128(tmp)

def mm_loadl_pi(a: M128, mem_addr: (bytes, int)) -> M128:
    val1 = read_float(mem_addr)
    data, start = mem_addr
    val2 = read_float((data, start+4))
    tmp = a.get_floats().copy()
    tmp[0] = val1
    tmp[1] = val2
    return M128(tmp)

def mm_loadr_ps(mem_addr: (bytes, int)) -> M128:
    vals = reversed(mm_load_ps(mem_addr).get_floats())
    return M128(list(vals))

def mm_loadu_ps(mem_addr: (bytes, int)) -> M128:
    return mm_load_ps(mem_addr)

def mm_malloc(_, _1) -> None:
    raise NotImplementedError("Function not implemented.")

def mm_max_ps(a: M128, b: M128) -> M128:
    return M128(np.maximum(a.get_floats(), b.get_floats()))

def mm_max_ss(a: M128, b: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = max(a.get_first(), b.get_first())
    return M128(tmp)

def mm_min_ps(a: M128, b: M128) -> M128:
    return M128(np.minimum(a.get_floats(), b.get_floats()))

def mm_min_ss(a: M128, b: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = min(a.get_first(), b.get_first())
    return M128(tmp)

def mm_move_ss(a: M128, b: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = b.get_first()
    return M128(tmp)

def mm_movehl_ps(a: M128, b: M128) -> M128:
    tmp = a.get_floats().copy()
    bs = b.get_floats()
    tmp[0] = bs[2]
    tmp[1] = bs[3]
    return M128(tmp)

def mm_movelh_ps(a: M128, b: M128) -> M128:
    tmp = a.get_floats().copy()
    bs = b.get_floats()
    tmp[2] = bs[0]
    tmp[3] = bs[1]
    return M128(tmp)

def mm_movemask_ps(a: M128) -> int:
    return vector2mask(a.as_uints(32))

def mm_mul_ps(a: M128, b: M128) -> M128:
    return M128(a.get_floats() * b.get_floats())

def mm_mul_ss(a: M128, b: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = tmp[0] * b.get_first()
    return M128(tmp)

def mm_or_ps(a: M128, b: M128) -> M128:
    return M128((a.as_uints() | b.as_uints()).view(np.float32))

def mm_prefetch(_, _1) -> None:
    raise NotImplementedError("Function not implemented.")

def mm_rcp_ps(a: M128) -> M128:
    return M128(1 / a.get_floats())

def mm_rcp_ss(a: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = 1 / tmp[0]
    return M128(tmp)

def mm_rsqrt_ps(a: M128) -> M128:
    return mm_rcp_ps(mm_sqrt_ps(a))

def mm_rsqrt_ss(a: M128) -> M128:
    return mm_rcp_ss(mm_sqrt_ss(a))

def MM_SET_EXCEPTION_MASK(a: int) -> None:
    raise NotImplementedError("Function not implemented.")

def MM_SET_EXCEPTION_STATE(a: int) -> None:
    raise NotImplementedError("Function not implemented.")

def MM_SET_FLUSH_ZERO_MODE(a: int) -> None:
    raise NotImplementedError("Function not implemented.")

def mm_set_ps(e3: float, e2: float, e1: float, e0: float) -> M128:
    return M128(np.array([e0, e1, e2, e3], dtype=np.float32))

def mm_set_ps1(a: float) -> M128:
    return M128(np.array([a, a, a, a], dtype=np.float32))

def MM_SET_ROUNDING_MODE(a: int) -> None:
    raise NotImplementedError("Function not implemented.")

def mm_set_ss(a: float) -> M128:
    tmp = mm_setzero_ps()
    tmp.set_first(a)
    return tmp

def mm_set1_ps(a: float) -> M128:
    return mm_set_ps1(a)

def mm_setcsr(_: int) -> None:
    raise NotImplementedError("Function not implemented.")

def mm_setr_ps(e3: float, e2: float, e1: float, e0: float) -> M128:
    return mm_set_ps(e0, e1, e2, e3)

def mm_setzero_ps() -> M128:
    return M128(np.zeros((4,), dtype=np.float32))

def mm_sfence() -> None:
    raise NotImplementedError("Function not implemented.")

def mm_shuffle_ps(a: M128, b: M128, imm8: int) -> M128:
    dst = mm_undefined_ps()
    dst_a = dst.get_floats()
    tmp1 = a.get_floats()
    tmp2 = b.get_floats()
    dst_a[0] = tmp1[imm8 & 3]
    dst_a[1] = tmp1[(imm8 >> 2) & 3]
    dst_a[2] = tmp2[(imm8 >> 4) & 3]
    dst_a[3] = tmp2[(imm8 >> 6) & 3]
    return dst

def mm_sqrt_ps(a: M128) -> M128:
    return M128(np.sqrt(a.get_floats()))

def mm_sqrt_ss(a: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = np.sqrt(tmp[0])
    return M128(tmp)

def mm_store_ps(mem_addr: (bytes, int), a: M128) -> None:
    write_floats(mem_addr, a.get_floats())

def mm_store_ps1(mem_addr: (bytes, int), a: M128) -> None:
    write_floats(mem_addr, [a.get_floats()[0]] * 4)

def mm_store_ss(mem_addr: (bytes, int), a: M128) -> None:
    write_floats(mem_addr, [a.get_first()])

def mm_store1_ps(mem_addr: (bytes, int), a: M128) -> None:
    mm_store_ps1(mem_addr, a)

def mm_storeh_pi(mem_addr: (bytes, int), a: M128) -> None:
    write_floats(mem_addr, a.get_floats()[2:])

def mm_storel_pi(mem_addr: (bytes, int), a: M128) -> None:
    write_floats(mem_addr, a.get_floats()[:2])

def mm_storer_ps(mem_addr: (bytes, int), a: M128) -> None:
    tmp = list(reversed(a.get_floats()))
    write_floats(mem_addr, tmp)

def mm_storeu_ps(mem_addr: (bytes, int), a: M128) -> None:
    mm_store_ps(mem_addr, a)

def mm_stream_ps(mem_addr: (bytes, int), a: M128) -> None:
    mm_store_ps(mem_addr, a)

def mm_sub_ps(a: M128, b: M128) -> M128:
    return M128(a.get_floats() - b.get_floats())

def mm_sub_ss(a: M128, b: M128) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = tmp[0] - b.get_first()
    return M128(tmp)

def MM_TRANSPOSE4_PS(row0: M128, row1: M128, row2: M128, row3: M128) -> (M128, M128, M128, M128):
    tmp0 = mm_unpacklo_ps(row0, row1)
    tmp2 = mm_unpacklo_ps(row2, row3)
    tmp1 = mm_unpackhi_ps(row0, row1)
    tmp3 = mm_unpackhi_ps(row2, row3)
    dst0 = mm_movelh_ps(tmp0, tmp2)
    dst1 = mm_movehl_ps(tmp2, tmp0)
    dst2 = mm_movelh_ps(tmp1, tmp3)
    dst3 = mm_movehl_ps(tmp3, tmp1)
    return dst0, dst1, dst2, dst3

def mm_ucomieq_ss(a: M128, b: M128) -> int:
    return mm_comieq_ss(a, b)

def mm_ucomige_ss(a: M128, b: M128) -> int:
    return mm_comige_ss(a, b)

def mm_ucomigt_ss(a: M128, b: M128) -> int:
    return mm_comigt_ss(a, b)

def mm_ucomile_ss(a: M128, b: M128) -> int:
    return mm_comile_ss(a, b)

def mm_ucomilt_ss(a: M128, b: M128) -> int:
    return mm_comilt_ss(a, b)

def mm_ucomineq_ss(a: M128, b: M128) -> int:
    return mm_comineq_ss(a, b)

def mm_undefined_ps() -> M128:
    return M128(np.empty((4,), dtype=np.float32))

def mm_unpackhi_ps(a: M128, b: M128) -> M128:
    tmp = b.get_floats().copy()
    aa = a.get_floats()
    tmp[0] = aa[2]
    tmp[1] = tmp[2]
    tmp[2] = aa[3]
    return M128(tmp)

def mm_unpacklo_ps(a: M128, b: M128) -> M128:
    tmp = a.get_floats().copy()
    bb = b.get_floats()
    tmp[2] = tmp[1]
    tmp[1] = bb[0]
    tmp[3] = bb[1]
    return M128(tmp)

def mm_xor_ps(a: M128, b: M128) -> M128:
    return M128((a.as_uints() ^ b.as_uints()).view(np.float32))

# =================== M64 ===================
# Not supported functions of the packed M64 type

def mm_avg_pu16(a: M64, b: M64) -> M64:
    pass
def mm_avg_pu8(a: M64, b: M64) -> M64:
    pass
def mm_cvt_pi2ps(a: M128, b: M64) -> M128:
    pass
def mm_cvt_ps2pi(a: M128) -> M64:
    pass
def mm_cvtpi16_ps(a: M64) -> M128:
    pass
def mm_cvtpi32_ps(a: M128, b: M64) -> M128:
    pass
def mm_cvtpi32x2_ps(a: M64, b: M64) -> M128:
    pass
def mm_cvtpi8_ps(a: M64) -> M128:
    pass
def mm_cvtps_pi16(a: M128) -> M64:
    pass
def mm_cvtps_pi32(a: M128) -> M64:
    pass
def mm_cvtps_pi8(a: M128) -> M64:
    pass
def mm_cvtpu16_ps(a: M64) -> M128:
    pass
def mm_cvtpu8_ps(a: M64) -> M128:
    pass
def mm_cvtt_ps2pi(a: M128) -> M64:
    pass
def mm_cvttps_pi32(a: M128) -> M64:
    pass
def mm_extract_pi16(a: M64, imm8: int) -> int:
    pass
def mm_insert_pi16(a: M64, i: int, imm8: int) -> M64:
    pass
def mm_maskmove_si64(a: M64, mask: M64, mem_addr: (bytes, int)) -> None:
    pass
def m_maskmovq(a: M64, mask: M64, mem_addr: (bytes, int)) -> None:
    pass
def mm_max_pi16(a: M64, b: M64) -> M64:
    pass
def mm_max_pu8(a: M64, b: M64) -> M64:
    pass
def mm_min_pi16(a: M64, b: M64) -> M64:
    pass
def mm_min_pu8(a: M64, b: M64) -> M64:
    pass
def mm_movemask_pi8(a: M64) -> int:
    pass
def mm_mulhi_pu16(a: M64, b: M64) -> M64:
    pass
def m_pavgb(a: M64, b: M64) -> M64:
    pass
def m_pavgw(a: M64, b: M64) -> M64:
    pass
def m_pextrw(a: M64, imm8: int) -> int:
    pass
def m_pinsrw(a: M64, i: int, imm8: int) -> M64:
    pass
def m_pmaxsw(a: M64, b: M64) -> M64:
    pass
def m_pmaxub(a: M64, b: M64) -> M64:
    pass
def m_pminsw(a: M64, b: M64) -> M64:
    pass
def m_pminub(a: M64, b: M64) -> M64:
    pass
def m_pmovmskb(a: M64) -> int:
    pass
def m_pmulhuw(a: M64, b: M64) -> M64:
    pass
def m_psadbw(a: M64, b: M64) -> M64:
    pass
def m_pshufw(a: M64, imm8: int) -> M64:
    pass
def mm_sad_pu8(a: M64, b: M64) -> M64:
    pass
def mm_shuffle_pi16(a: M64, imm8: int) -> M64:
    pass
def mm_stream_pi(mem_addr: (bytes, int), a: M64) -> None:
    pass
