from __future__ import annotations
import struct

import numpy as np

from SSE import M64, M128, mm_setzero_ps, Lane
from utility_functions import *


class M128I(Lane):
    def __init__(self, elements: np.ndarray):
        if not isinstance(elements, (list, np.ndarray)):
            raise TypeError("Expected a numpy.ndarray, got:", elements.__class__)
        self._ele = elements

    def copy(self) -> M128I:
        return M128I(self._ele.copy())
        
    def get_i8(self) -> np.ndarray:
        return self._ele.view(np.int8)
    def get_u8(self) -> np.ndarray:
        return self._ele.view(np.uint8)
    def get_i16(self) -> np.ndarray:
        return self._ele.view(np.int16)
    def get_u16(self) -> np.ndarray:
        return self._ele.view(np.uint16)
    def get_i32(self) -> np.ndarray:
        return self._ele.view(np.int32)
    def get_u32(self) -> np.ndarray:
        return self._ele.view(np.uint32)
    def get_i64(self) -> np.ndarray:
        return self._ele.view(np.int64)
    def get_u64(self) -> np.ndarray:
        return self._ele.view(np.uint64)

class M128D(Lane):
    def __init__(self, elements: np.ndarray):
        self._ele = elements
    def get_doubles(self) -> np.ndarray:
        return self._ele
    def get_first(self) -> np.float64:
        return self._ele[0]
    def as_uints(self, size=64) -> np.ndarray:
        return self._ele.view(size2utype(size))
    def copy(self) -> M128D:
        return M128D(self._ele.copy())
    def set_first(self, value) -> None:
        self._ele[0] = value


#### helper functions
def join_arrays(*args):
    for a in args:
        for j in a:
            yield j



#### Intrinsics
def mm_add_epi16(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i16() + b.get_i16())

def mm_add_epi32(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i32() + b.get_i32())

def mm_add_epi64(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i64() + b.get_i64())

def mm_add_epi8(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i8() + b.get_i8())

def mm_add_pd(a: M128D, b: M128D) -> M128D:
    return M128D(a.get_doubles() + b.get_doubles())

def mm_add_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = a.get_first() + b.get_first()
    return M128D(tmp)

def mm_adds_epi16(a: M128I, b: M128I) -> M128I:
    tmp = a.get_i16() + b.get_i16().astype(np.int32)
    return M128I(saturation(tmp, np.int16))

def mm_adds_epi8(a: M128I, b: M128I) -> M128I:
    tmp = a.get_i8() + b.get_i8().astype(np.int16)
    return M128I(saturation(tmp, np.int8))

def mm_adds_epu16(a: M128I, b: M128I) -> M128I:
    tmp = a.get_u16() + b.get_u16().astype(np.uint32)
    return M128I(saturation(tmp, np.uint16))

def mm_adds_epu8(a: M128I, b: M128I) -> M128I:
    tmp = a.get_u8() + b.get_u8().astype(np.uint16)
    return M128I(saturation(tmp, np.uint8))

def mm_and_pd(a: M128D, b: M128D) -> M128D:
    return M128D((a.as_uints() & b.as_uints()).view(np.float64))

def mm_and_si128(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i64() & b.get_i64())

def mm_andnot_pd(a: M128D, b: M128D) -> M128D:
    return M128D((~a.as_uints() & b.as_uints()).view(np.float64))

def mm_andnot_si128(a: M128I, b: M128I) -> M128I:
    return M128I(~a.get_i64() & b.get_i64())

def mm_avg_epu16(a: M128I, b: M128I) -> M128I:
    mx = np.maximum(a.get_u16(), b.get_u16())
    mn = np.minimum(a.get_u16(), b.get_u16())
    tmp = mx - mn
    ones = tmp & 1
    return M128I(mn + ones + tmp // 2)

def mm_avg_epu8(a: M128I, b: M128I) -> M128I:
    mx = np.maximum(a.get_u8(), b.get_u8())
    mn = np.minimum(a.get_u8(), b.get_u8())
    tmp = mx - mn
    ones = tmp & 1
    return M128I(mn + ones + tmp // 2)

def mm_bslli_si128(a: M128I, imm8: int) -> M128I:
    imm8 = min(imm8, 16)
    tmp = np.zeros((imm8,), dtype=np.uint8)
    return M128I(np.concatenate((tmp, a.get_u8()[:16-imm8])))

def mm_bsrli_si128(a: M128I, imm8: int) -> M128I:
    imm8 = min(imm8, 16)
    tmp = np.zeros((imm8,), dtype=np.uint8)
    return M128I(np.concatenate((a.get_u8()[imm8:], tmp)))

def mm_castpd_ps(a: M128D) -> M128:
    return M128(a.get_doubles().view(np.float32))

def mm_castpd_si128(a: M128D) -> M128I:
    return M128I(a.get_doubles().view(np.int64))

def mm_castps_pd(a: M128) -> M128D:
    return M128D(a.get_floats().view(np.float64))

def mm_castps_si128(a: M128) -> M128I:
    return M128I(a.get_floats().view(np.int32))

def mm_castsi128_pd(a: M128I) -> M128D:
    return M128D(a.get_i64().view(np.float64))

def mm_castsi128_ps(a: M128I) -> M128:
    return M128(a.get_i32().view(np.float32))

def mm_clflush(p: None) -> None:
    raise NotImplementedError("Function not implemented.")

def mm_cmpeq_epi16(a: M128I, b: M128I) -> M128I:
    return M128I(bools_to_mask(a.get_i16() == b.get_i16(), 16))

def mm_cmpeq_epi32(a: M128I, b: M128I) -> M128I:
    return M128I(bools_to_mask(a.get_i32() == b.get_i32(), 32))

def mm_cmpeq_epi8(a: M128I, b: M128I) -> M128I:
    return M128I(bools_to_mask(a.get_i8() == b.get_i8(), 8))

def mm_cmpeq_pd(a: M128D, b: M128D) -> M128D:
    return M128D(bools_to_mask(a.get_doubles() == b.get_doubles(), 64).view(np.float64))

def mm_cmpeq_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp.view(np.uint64)[0] = bool2mask(tmp[0] == b.get_first(), 64)
    return M128D(tmp)

def mm_cmpge_pd(a: M128D, b: M128D) -> M128D:
    return M128D(bools_to_mask(a.get_doubles() >= b.get_doubles(), 64).view(np.float64))

def mm_cmpge_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp.view(np.uint64)[0] = bool2mask(tmp[0] >= b.get_first(), 64)
    return M128D(tmp)

def mm_cmpgt_epi16(a: M128I, b: M128I) -> M128I:
    return M128I(bools_to_mask(a.get_i16() > b.get_i16(), 16))

def mm_cmpgt_epi32(a: M128I, b: M128I) -> M128I:
    return M128I(bools_to_mask(a.get_i32() > b.get_i32(), 32))

def mm_cmpgt_epi8(a: M128I, b: M128I) -> M128I:
    return M128I(bools_to_mask(a.get_i8() > b.get_i8(), 8))

def mm_cmpgt_pd(a: M128D, b: M128D) -> M128D:
    return M128D(bools_to_mask(a.get_doubles() > b.get_doubles(), 64).view(np.float64))

def mm_cmpgt_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp.view(np.uint64)[0] = bool2mask(tmp[0] > b.get_first(), 64)
    return M128D(tmp)

def mm_cmple_pd(a: M128D, b: M128D) -> M128D:
    return mm_cmpge_pd(b, a)

def mm_cmple_sd(a: M128D, b: M128D) -> M128D:
    return mm_cmpge_sd(b, a)

def mm_cmplt_epi16(a: M128I, b: M128I) -> M128I:
    return mm_cmpgt_epi16(b, a)

def mm_cmplt_epi32(a: M128I, b: M128I) -> M128I:
    return mm_cmpgt_epi32(b, a)

def mm_cmplt_epi8(a: M128I, b: M128I) -> M128I:
    return mm_cmpgt_epi8(b, a)

def mm_cmplt_pd(a: M128D, b: M128D) -> M128D:
    return mm_cmpgt_pd(b, a)

def mm_cmplt_sd(a: M128D, b: M128D) -> M128D:
    return mm_cmpgt_sd(b, a)

def mm_cmpneq_pd(a: M128D, b: M128D) -> M128D:
    return M128D(bools_to_mask(a.get_doubles() != b.get_doubles(), 64).view(np.float64))

def mm_cmpneq_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp.view(np.uint64)[0] = bool2mask(tmp[0] != b.get_first(), 64)
    return M128D(tmp)

def mm_cmpnge_pd(a: M128D, b: M128D) -> M128D:
    return mm_cmplt_pd(a, b)

def mm_cmpnge_sd(a: M128D, b: M128D) -> M128D:
    return mm_cmplt_sd(a, b)

def mm_cmpngt_pd(a: M128D, b: M128D) -> M128D:
    return mm_cmple_pd(a, b)

def mm_cmpngt_sd(a: M128D, b: M128D) -> M128D:
    return mm_cmple_sd(a, b)

def mm_cmpnle_pd(a: M128D, b: M128D) -> M128D:
    return mm_cmpgt_pd(a, b)

def mm_cmpnle_sd(a: M128D, b: M128D) -> M128D:
    return mm_cmpgt_sd(a, b)

def mm_cmpnlt_pd(a: M128D, b: M128D) -> M128D:
    return mm_cmpge_pd(a, b)

def mm_cmpnlt_sd(a: M128D, b: M128D) -> M128D:
    return mm_cmpge_sd(a, b)

def mm_cmpord_pd(a: M128D, b: M128D) -> M128D:
    return M128D(bools_to_mask(not np.isnan(a.get_doubles()) and
                               not np.isnan(b.get_doubles()), 64).view(np.float64))

def mm_cmpord_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    b = not np.isnan(tmp[0]) and not np.isnan(b.get_first())
    tmp.view(np.uint64)[0] = bool2mask(b, 64)
    return tmp

def mm_cmpunord_pd(a: M128D, b: M128D) -> M128D:
    tmp = np.isnan(a.get_doubles()) or np.isnan(b.get_doubles())
    return M128D(bools_to_mask(tmp, 64).view(np.float64))

def mm_cmpunord_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    b = np.isnan(a.get_first()) or np.isnan(b.get_first())
    tmp.view(np.uint64)[0] = bool2mask(b, 64)
    return tmp

def mm_comieq_sd(a: M128D, b: M128D) -> int:
    return 1 if a.get_first() == b.get_first() else 0

def mm_comige_sd(a: M128D, b: M128D) -> int:
    return 1 if a.get_first() >= b.get_first() else 0

def mm_comigt_sd(a: M128D, b: M128D) -> int:
    return 1 if a.get_first() > b.get_first() else 0

def mm_comile_sd(a: M128D, b: M128D) -> int:
    return mm_comige_sd(b, a)

def mm_comilt_sd(a: M128D, b: M128D) -> int:
    return mm_comigt_sd(b, a)

def mm_comineq_sd(a: M128D, b: M128D) -> int:
    return 1 - mm_comieq_sd(b, a)

def mm_cvtepi32_pd(a: M128I) -> M128D:
    return M128D(a.get_i32()[:2].astype(np.float32))

def mm_cvtepi32_ps(a: M128I) -> M128:
    return M128(a.get_i32().astype(np.float32))

def mm_cvtpd_epi32(a: M128D) -> M128I:
    dst = mm_setzero_si128()
    tmp = a.get_doubles().astype(np.int32)
    dst.get_i32()[0] = tmp[0]
    dst.get_i32()[1] = tmp[1]
    return dst

def mm_cvtpd_ps(a: M128D) -> M128:
    dst = mm_setzero_ps()
    tmp = a.get_doubles().astype(np.float32)
    dst.get_floats()[0] = tmp[0]
    dst.get_floats()[1] = tmp[1]
    return dst

def mm_cvtps_epi32(a: M128) -> M128I:
    return M128I(a.get_floats().astype(np.int32))

def mm_cvtps_pd(a: M128) -> M128D:
    return M128D(a.get_floats()[:2].astype(np.float64))

def mm_cvtsd_f64(a: M128D) -> float:
    return a.get_first()

def mm_cvtsd_si32(a: M128D) -> int:
    return int(a.get_first()) & bool2mask(True, 32)

def mm_cvtsd_si64(a: M128D) -> int:
    return int(a.get_first()) & bool2mask(True, 64)

def mm_cvtsd_si64x(a: M128D) -> int:
    return mm_cvtsd_si64(a)

def mm_cvtsd_ss(a: M128, b: M128D) -> M128:
    tmp = a.get_floats().copy()
    tmp[0] = b.get_first().astype(np.float32)
    return M128(tmp)

def mm_cvtsi128_si32(a: M128I) -> int:
    return a.get_i32()[0]

def mm_cvtsi128_si64(a: M128I) -> int:
    return a.get_i64()[0]

def mm_cvtsi128_si64x(a: M128I) -> int:
    return mm_cvtsi128_si64(a)

def mm_cvtsi32_sd(a: M128D, b: int) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = b
    return M128D(tmp)

def mm_cvtsi32_si128(a: int) -> M128I:
    tmp = mm_setzero_si128()
    tmp.get_i32()[0] = a
    return tmp

def mm_cvtsi64_sd(a: M128D, b: int) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = b
    return M128D(tmp)

def mm_cvtsi64_si128(a: int) -> M128I:
    tmp = mm_setzero_si128()
    tmp.get_i64()[0] = a
    return tmp

def mm_cvtsi64x_sd(a: M128D, b: int) -> M128D:
    return mm_cvtsi64_sd(a, b)

def mm_cvtsi64x_si128(a: int) -> M128I:
    return mm_cvtsi64_si128(a)

def mm_cvtss_sd(a: M128D, b: M128) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = b.get_first()
    return M128D(tmp)

def mm_cvttpd_epi32(a: M128D) -> M128I:
    return mm_cvtpd_epi32(a)

def mm_cvttpd_pi32(a: M128D) -> M64:
    return mm_cvtpd_pi32(a)

def mm_cvttps_epi32(a: M128) -> M128I:
    return mm_cvtps_epi32(a)

def mm_cvttsd_si32(a: M128D) -> int:
    return mm_cvtsd_si32(a)

def mm_cvttsd_si64(a: M128D) -> int:
    return mm_cvtsd_si64(a)

def mm_cvttsd_si64x(a: M128D) -> int:
    return mm_cvtsd_si64x(a)

def mm_div_pd(a: M128D, b: M128D) -> M128D:
    return M128D(a.get_doubles() / b.get_doubles())

def mm_div_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = a.get_first() / b.get_first()
    return M128D(tmp)

def mm_extract_epi16(a: M128I, imm8: int) -> int:
    return a.get_i16()[imm8 & 7]

def mm_insert_epi16(a: M128I, i: int, imm8: int) -> M128I:
    tmp = a.get_i16().copy()
    tmp[imm8 & 7] = i
    return M128I(tmp)

def mm_lfence() -> None:
    raise NotImplementedError("Function not implemented.")

def mm_load_pd(mem_addr: (bytes, int)) -> M128D:
    data, start = mem_addr
    v1 = read_double(mem_addr)
    v2 = read_double((data, start+8))
    return mm_set_pd(v2, v1)

def mm_load_pd1(mem_addr: (bytes, int)) -> M128D:
    return mm_set1_pd(read_double(mem_addr))

def mm_load_sd(mem_addr: (bytes, int)) -> M128D:
    return mm_set_sd(read_double(mem_addr))

def mm_load_si128(mem_addr: (bytes, int)) -> M128I:
    data, start = mem_addr
    return mm_setr_epi8(*data[start:start+16])

def mm_load1_pd(mem_addr: (bytes, int)) -> M128D:
    return mm_load_pd1(mem_addr)

def mm_loadh_pd(a: M128D, mem_addr: (bytes, int)) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[1] = read_double(mem_addr)
    return M128D(tmp)

def mm_loadl_epi64(mem_addr: (bytes, int)) -> M128I:
    tmp = mm_setzero_si128()
    tmp.get_i64()[0] = read_int(mem_addr, 64)
    return tmp

def mm_loadl_pd(a: M128D, mem_addr: (bytes, int)) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = read_double(mem_addr)
    return M128D(tmp)

def mm_loadr_pd(mem_addr: (bytes, int)) -> M128D:
    data, start = mem_addr
    v1 = read_double(mem_addr)
    v2 = read_double((data, start+8))
    return mm_set_pd(v1, v2)

def mm_loadu_pd(mem_addr: (bytes, int)) -> M128D:
    return mm_load_pd(mem_addr)

def mm_loadu_si128(mem_addr: (bytes, int)) -> M128I:
    return mm_load_si128(mem_addr)

def mm_loadu_si16(mem_addr: (bytes, int)) -> M128I:
    tmp = mm_setzero_si128()
    tmp.get_i16()[0] = read_int(mem_addr, 16)
    return tmp

def mm_loadu_si32(mem_addr: (bytes, int)) -> M128I:
    tmp = mm_setzero_si128()
    tmp.get_i32()[0] = read_int(mem_addr, 32)
    return tmp

def mm_loadu_si64(mem_addr: (bytes, int)) -> M128I:
    return mm_loadl_epi64(mem_addr)

def mm_madd_epi16(a: M128I, b: M128I) -> M128I:
    tmp = a.get_i16() * b.get_i16().astype(np.int32)
    return M128I(hadd(tmp))

def mm_maskmoveu_si128(a: M128I, mask: M128I, mem_addr: (bytes, int)) -> None:
    pass

def mm_max_epi16(a: M128I, b: M128I) -> M128I:
    return M128I(np.maximum(a.get_i16(), b.get_i16()))

def mm_max_epu8(a: M128I, b: M128I) -> M128I:
    return M128I(np.maximum(a.get_u8(), b.get_u8()))

def mm_max_pd(a: M128D, b: M128D) -> M128D:
    return M128D(np.maximum(a.get_doubles(), b.get_doubles()))

def mm_max_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = np.maximum(a.get_first(), b.get_first())
    return M128D(tmp)

def mm_mfence() -> None:
    raise NotImplementedError("Function not implemented.")

def mm_min_epi16(a: M128I, b: M128I) -> M128I:
    return M128I(np.minimum(a.get_i16(), b.get_i16()))

def mm_min_epu8(a: M128I, b: M128I) -> M128I:
    return M128I(np.minimum(a.get_u8(), b.get_u8()))

def mm_min_pd(a: M128D, b: M128D) -> M128D:
    return M128D(np.minimum(a.get_doubles(), b.get_doubles()))

def mm_min_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = np.minimum(a.get_first(), b.get_first())
    return M128D(tmp)

def mm_move_epi64(a: M128I) -> M128I:
    tmp = a.get_i64().copy()
    tmp[1] = 0
    return M128I(tmp)

def mm_move_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = b.get_first()
    return M128D(tmp)

def mm_movemask_epi8(a: M128I) -> int:
    dst = 0
    for i in a.get_u8():
        dst = dst << 1
        if i & 0x80:
            dst += 1
    return dst
        
def mm_movemask_pd(a: M128D) -> int:
    dst = 0
    for i in a.get_doubles().view(np.uint64):
        dst = dst << 1
        if i & 2**63:
            dst += 1
    return dst

def mm_mul_epu32(a: M128I, b: M128I) -> M128I:
    mx = np.iinfo(np.uint32).max
    tmp = (a.get_u64() & mx) * (b.get_u64() & mx)
    return M128I(tmp)

def mm_mul_pd(a: M128D, b: M128D) -> M128D:
    return M128D(a.get_doubles() * b.get_doubles())

def mm_mul_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = a.get_first() * b.get_first()
    return M128D(tmp)

def mm_mulhi_epi16(a: M128I, b: M128I) -> M128I:
    tmp1 = a.get_i16()
    tmp2 = b.get_i16()
    dst = mm_setzero_si128()
    dst_a = dst.get_i16()
    for n, z in enumerate(zip(tmp1, tmp2)):
        i, j = z
        dst_a[n] = (i * j.astype(np.int32)) >> 16
    return dst

def mm_mulhi_epu16(a: M128I, b: M128I) -> M128I:
    tmp1 = a.get_u16()
    tmp2 = b.get_u16()
    dst = mm_setzero_si128()
    dst_a = dst.get_u16()
    for n, z in enumerate(zip(tmp1, tmp2)):
        i, j = z
        dst_a[n] = (i * j.astype(np.uint32)) >> 16
    return dst

def mm_mullo_epi16(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i16() * b.get_i16())

def mm_or_pd(a: M128D, b: M128D) -> M128D:
    return M128D((a.as_uints() | b.as_uints()).view(np.float64))

def mm_or_si128(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i64() | b.get_i64())

def mm_packs_epi16(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i16(), b.get_i16()))
    return M128I(saturation(tmp, np.int8))
        
def mm_packs_epi32(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i32(), b.get_i32()))
    return M128I(saturation(tmp, np.int16))

def mm_packus_epi16(a: M128I, b: M128I) -> M128I:
    tmp = np.concatenate((a.get_i16(), b.get_i16()))
    return M128I(saturation(tmp, np.uint8))

def mm_pause() -> None:
    return

def mm_sad_epu8(a: M128I, b: M128I) -> M128I:
    tmp = np.absolute(a.get_u8() - b.get_u8().astype(np.int16))
    dst = mm_setzero_si128()
    dst_a = dst.get_u64()
    for n, i in enumerate(tmp):
        dst_a[n // 8] += i
    return dst
    
def mm_set_epi16(e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M128I:
    arr = [e0, e1, e2, e3, e4, e5, e6, e7]
    tmp = np.array(arr, dtype=np.int16)
    return M128I(tmp)

def mm_set_epi32(e3: int, e2: int, e1: int, e0: int) -> M128I:
    return M128I(np.array([e0, e1, e2, e3], dtype=np.int32))

def mm_set_epi64x(e1: int, e0: int) -> M128I:
    return M128I(np.array([e0, e1], dtype=np.int64))

def mm_set_epi8(e15: int, e14: int, e13: int, e12: int, e11: int, e10: int, e9: int, e8: int, e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M128I:
    arr = [e0, e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15]
    tmp = np.array(arr, dtype=np.int8)
    return M128I(tmp)

def mm_set_pd(e1: float, e0: float) -> M128D:
    return M128D(np.array([e0, e1], dtype=np.float64))

def mm_set_pd1(a: float) -> M128D:
    return mm_set_pd(a, a)

def mm_set_sd(a: float) -> M128D:
    return mm_set_pd(0.0, a)

def mm_set1_epi16(a: int) -> M128I:
    return mm_set_epi16(a, a, a, a, a, a, a, a)

def mm_set1_epi32(a: int) -> M128I:
    return mm_set_epi32(a, a, a, a)

def mm_set1_epi64x(a: int) -> M128I:
    return mm_set_epi64x(a, a)

def mm_set1_epi8(a: int) -> M128I:
    return mm_set_epi8(a, a, a, a, a, a, a, a, a, a, a, a, a, a, a, a)

def mm_set1_pd(a: float) -> M128D:
    return mm_set_pd(a, a)

def mm_setr_epi16(e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M128I:
    return mm_set_epi16(e0, e1, e2, e3, e4, e5, e6, e7)

def mm_setr_epi32(e3: int, e2: int, e1: int, e0: int) -> M128I:
    return mm_set_epi32(e0, e1, e2, e3)

def mm_setr_epi64(e1: int, e0: int) -> M128I:
    return mm_set_epi64x(e0, e1)

def mm_setr_epi8(e15: int, e14: int, e13: int, e12: int, e11: int, e10: int, e9: int, e8: int, e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M128I:
    return mm_set_epi8(e0, e1, e2,  e3,  e4,  e5,  e6,  e7,
                       e8, e9, e10, e11, e12, e13, e14, e15)

def mm_setr_pd(e1: float, e0: float) -> M128D:
    return mm_set_pd(e0, e1)

def mm_setzero_pd() -> M128D:
    return M128D(np.zeros((2, ), dtype=np.float64))

def mm_setzero_si128() -> M128I:
    return M128I(np.zeros((2, ), dtype=np.int64))

def mm_shuffle_epi32(a: M128I, imm8: int) -> M128I:
    dst = mm_undefined_si128()
    dst_a = dst.get_i32()
    tmp = a.get_i32()
    for i in range(4):
        j = imm8 & 0x3
        imm8 = imm8 >> 2
        dst_a[i] = tmp[j]
    return dst

def mm_shuffle_pd(a: M128D, b: M128D, imm8: int) -> M128D:
    dst = mm_undefined_pd()
    dst_a = dst.get_doubles()
    tmp1 = a.get_doubles()
    tmp2 = b.get_doubles()
    dst_a[0] = tmp1[1] if (imm8 & 0x1) else tmp1[0]
    dst_a[1] = tmp2[1] if (imm8 & 0x2) else tmp2[0]
    return dst

def mm_shufflehi_epi16(a: M128I, imm8: int) -> M128I:
    dst = a.copy()
    dst_a = dst.get_i16()
    tmp = a.get_i16()
    for i in range(4):
        dst_a[4 + i] = tmp[4 + (imm8 & 3)]
        imm8 = imm8 >> 2
    return dst

def mm_shufflelo_epi16(a: M128I, imm8: int) -> M128I:
    dst = a.copy()
    dst_a = dst.get_i16()
    tmp = a.get_i16()
    for i in range(4):
        dst_a[i] = tmp[imm8 & 3]
        imm8 = imm8 >> 2
    return dst

def mm_sll_epi16(a: M128I, count: M128I) -> M128I:
    return mm_slli_epi16(a, count.get_u64()[0])

def mm_sll_epi32(a: M128I, count: M128I) -> M128I:
    return mm_slli_epi32(a, count.get_u64()[0])

def mm_sll_epi64(a: M128I, count: M128I) -> M128I:
    return mm_slli_epi64(a, count.get_u64()[0])

def mm_slli_epi16(a: M128I, imm8: int) -> M128I:
    if 15 < imm8:
        return mm_setzero_si128()
    return M128I(a.get_u16() << imm8)

def mm_slli_epi32(a: M128I, imm8: int) -> M128I:
    if 31 < imm8:
        return mm_setzero_si128()
    return M128I(a.get_u32() << imm8)

def mm_slli_epi64(a: M128I, imm8: int) -> M128I:
    if 63 < imm8:
        return mm_setzero_si128()
    return M128I(a.get_u64() << imm8)

def mm_slli_si128(a: M128I, imm8: int) -> M128I:
    if 15 < imm8:
        return mm_setzero_si128()
    return mm_bslli_si128(a, imm8)
    
def mm_sqrt_pd(a: M128D) -> M128D:
    return M128D(np.sqrt(a.get_doubles()))

def mm_sqrt_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = np.sqrt(b.get_first())
    return M128D(tmp)

def mm_sra_epi16(a: M128I, count: M128I) -> M128I:
    return mm_srai_epi16(a, count.get_u64()[0])

def mm_sra_epi32(a: M128I, count: M128I) -> M128I:
    return mm_srai_epi32(a, count.get_u64()[0])

def mm_srai_epi16(a: M128I, imm8: int) -> M128I:
    imm8 = min(imm8, 16)
    return M128I(a.get_i16() >> imm8)

def mm_srai_epi32(a: M128I, imm8: int) -> M128I:
    imm8 = min(imm8, 32)
    return M128I(a.get_i32() >> imm8)

def mm_srl_epi16(a: M128I, count: M128I) -> M128I:
    return mm_srli_epi16(a, count.get_u64()[0])

def mm_srl_epi32(a: M128I, count: M128I) -> M128I:
    return mm_srli_epi32(a, count.get_u64()[0])

def mm_srl_epi64(a: M128I, count: M128I) -> M128I:
    return mm_srli_epi64(a, count.get_u64()[0])

def mm_srli_epi16(a: M128I, imm8: int) -> M128I:
    if 15 < imm8:
        return mm_setzero_si128()
    return M128I(a.get_u16() >> imm8)

def mm_srli_epi32(a: M128I, imm8: int) -> M128I:
    if 31 < imm8:
        return mm_setzero_si128()
    return M128I(a.get_u32() >> imm8)

def mm_srli_epi64(a: M128I, imm8: int) -> M128I:
    if 63 < imm8:
        return mm_setzero_si128()
    return M128I(a.get_u64() >> imm8)

def mm_srli_si128(a: M128I, imm8: int) -> M128I:
    if 15 < imm8:
        return mm_setzero_si128()
    return mm_bsrli_si128(a, imm8)

def mm_store_pd(mem_addr: (bytes, int), a: M128D) -> None:
    write_doubles(mem_addr, a.get_doubles())
    
def mm_store_pd1(mem_addr: (bytes, int), a: M128D) -> None:
    val = a.get_first()
    write_doubles(mem_addr, [val, val])
    
def mm_store_sd(mem_addr: (bytes, int), a: M128D) -> None:
    write_doubles(mem_addr, [a.get_first()])
    
def mm_store_si128(mem_addr: (bytes, int), a: M128I) -> None:
    write_int(mem_addr, a.get_u16())

def mm_store1_pd(mem_addr: (bytes, int), a: M128D) -> None:
    return mm_store_pd1(mem_addr, a)

def mm_storeh_pd(mem_addr: (bytes, int), a: M128D) -> None:
    write_doubles(mem_addr, [a.get_doubles()[0]])

def mm_storel_epi64(mem_addr: (bytes, int), a: M128I) -> None:
    write_int(mem_addr, a.get_u64()[:1])

def mm_storel_pd(mem_addr: (bytes, int), a: M128D) -> None:
    return mm_store1_pd(mem_addr, a)

def mm_storer_pd(mem_addr: (bytes, int), a: M128D) -> None:
    val1, val2 = a.get_doubles()
    write_doubles(mem_addr, [val2, val1])

def mm_storeu_pd(mem_addr: (bytes, int), a: M128D) -> None:
    return mm_store_pd(mem_addr, a)

def mm_storeu_si128(mem_addr: (bytes, int), a: M128I) -> None:
    return mm_store_si128(mem_addr, a)

def mm_storeu_si16(mem_addr: (bytes, int), a: M128I) -> None:
    write_int(mem_addr, a.get_u16()[:1])

def mm_storeu_si32(mem_addr: (bytes, int), a: M128I) -> None:
    write_int(mem_addr, a.get_u32()[:1])

def mm_storeu_si64(mem_addr: (bytes, int), a: M128I) -> None:
    return mm_storel_epi64(mem_addr, a)

def mm_stream_pd(mem_addr: (bytes, int), a: M128D) -> None:
    return mm_store_pd(mem_addr, a)

def mm_stream_si128(mem_addr: (bytes, int), a: M128I) -> None:
    return mm_store_si128(mem_addr, a)

def mm_stream_si32(mem_addr: (bytes, int), a: int) -> None:
    write_int(mem_addr, np.array([a], dtype=np.int32))

def mm_stream_si64(mem_addr: (bytes, int), a: int) -> None:
    write_int(mem_addr, np.array([a], dtype=np.int64))

def mm_sub_epi16(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i16() - b.get_i16())

def mm_sub_epi32(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i32() - b.get_i32())

def mm_sub_epi64(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i64() - b.get_i64())

def mm_sub_epi8(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i8() - b.get_i8())

def mm_sub_pd(a: M128D, b: M128D) -> M128D:
    return M128D(a.get_doubles() - b.get_doubles())

def mm_sub_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.get_doubles().copy()
    tmp[0] = tmp[0] - b.get_first()
    return M128D(tmp)

def mm_subs_epi16(a: M128I, b: M128I) -> M128I:
    tmp = a.get_i16() - b.get_i16().astype(np.int32)
    return M128I(saturation(tmp, np.int16))

def mm_subs_epi8(a: M128I, b: M128I) -> M128I:
    tmp = a.get_i8() - b.get_i8().astype(np.int16)
    return M128I(saturation(tmp, np.int8))

def mm_subs_epu16(a: M128I, b: M128I) -> M128I:
    tmp = a.get_u16() - b.get_u16().astype(np.int32)
    return M128I(saturation(tmp, np.uint16))

def mm_subs_epu8(a: M128I, b: M128I) -> M128I:
    tmp = a.get_u8() - b.get_u8().astype(np.int16)
    return M128I(saturation(tmp, np.uint8))

def mm_ucomieq_sd(a: M128D, b: M128D) -> int:
    return mm_comieq_sd(a, b)

def mm_ucomige_sd(a: M128D, b: M128D) -> int:
    return mm_comige_sd(a, b)

def mm_ucomigt_sd(a: M128D, b: M128D) -> int:
    return mm_comigt_sd(a, b)

def mm_ucomile_sd(a: M128D, b: M128D) -> int:
    return mm_comile_sd(a, b)

def mm_ucomilt_sd(a: M128D, b: M128D) -> int:
    return mm_comilt_sd(a, b)

def mm_ucomineq_sd(a: M128D, b: M128D) -> int:
    return mm_comineq_sd(a, b)

def mm_undefined_pd() -> M128D:
    return M128D(np.empty((2, ), dtype=np.float64))

def mm_undefined_si128() -> M128I:
    return M128I(np.empty((2, ), dtype=np.int64))

def mm_unpackhi_epi16(a: M128I, b: M128I) -> M128I:
    dst = mm_undefined_si128()
    dst_a = dst.get_i16()
    tmp1 = a.get_i16()
    tmp2 = b.get_i16()
    dst_a[0] = tmp1[4]
    dst_a[1] = tmp2[4]
    dst_a[2] = tmp1[5]
    dst_a[3] = tmp2[5]
    dst_a[4] = tmp1[6]
    dst_a[5] = tmp2[6]
    dst_a[6] = tmp1[7]
    dst_a[7] = tmp2[7]
    return dst

def mm_unpackhi_epi32(a: M128I, b: M128I) -> M128I:
    dst = mm_undefined_si128()
    dst_a = dst.get_i32()
    tmp1 = a.get_i32()
    tmp2 = b.get_i32()
    dst_a[0] = tmp1[2]
    dst_a[1] = tmp2[2]
    dst_a[2] = tmp1[3]
    dst_a[3] = tmp2[3]
    return dst

def mm_unpackhi_epi64(a: M128I, b: M128I) -> M128I:
    dst = mm_undefined_si128()
    dst_a = dst.get_i64()
    tmp1 = a.get_i64()
    tmp2 = b.get_i64()
    dst_a[0] = tmp1[1]
    dst_a[1] = tmp2[1]
    return dst

def mm_unpackhi_epi8(a: M128I, b: M128I) -> M128I:
    dst = mm_undefined_si128()
    dst_a = dst.get_i8()
    tmp1 = a.get_i8()
    tmp2 = b.get_i8()
    dst_a[0] = tmp1[8]
    dst_a[1] = tmp2[8]
    dst_a[2] = tmp1[9]
    dst_a[3] = tmp2[9]
    dst_a[4] = tmp1[10]
    dst_a[5] = tmp2[10]
    dst_a[6] = tmp1[11]
    dst_a[7] = tmp2[11]
    dst_a[8] = tmp1[12]
    dst_a[9] = tmp2[12]
    dst_a[10] = tmp1[13]
    dst_a[11] = tmp2[13]
    dst_a[12] = tmp1[14]
    dst_a[13] = tmp2[14]
    dst_a[14] = tmp1[15]
    dst_a[15] = tmp2[15]
    return dst

def mm_unpackhi_pd(a: M128D, b: M128D) -> M128D:
    dst = mm_undefined_pd()
    dst_a = dst.get_doubles()
    dst_a[0] = a.get_doubles()[1]
    dst_a[1] = b.get_doubles()[1]
    return dst

def mm_unpacklo_epi16(a: M128I, b: M128I) -> M128I:
    dst = mm_undefined_si128()
    dst_a = dst.get_i16()
    tmp1 = a.get_i16()
    tmp2 = b.get_i16()
    dst_a[0] = tmp1[0]
    dst_a[1] = tmp2[0]
    dst_a[2] = tmp1[1]
    dst_a[3] = tmp2[1]
    dst_a[4] = tmp1[2]
    dst_a[5] = tmp2[2]
    dst_a[6] = tmp1[3]
    dst_a[7] = tmp2[3]
    return dst

def mm_unpacklo_epi32(a: M128I, b: M128I) -> M128I:
    dst = mm_undefined_si128()
    dst_a = dst.get_i32()
    tmp1 = a.get_i32()
    tmp2 = b.get_i32()
    dst_a[0] = tmp1[0]
    dst_a[1] = tmp2[0]
    dst_a[2] = tmp1[1]
    dst_a[3] = tmp2[1]
    return dst

def mm_unpacklo_epi64(a: M128I, b: M128I) -> M128I:
    dst = a.get_i64().copy()
    dst[1] = b.get_i64()[0]
    return M128I(dst)

def mm_unpacklo_epi8(a: M128I, b: M128I) -> M128I:
    dst = mm_undefined_si128()
    dst_a = dst.get_i8()
    tmp1 = a.get_i8()
    tmp2 = b.get_i8()
    dst_a[0] = tmp1[0]
    dst_a[1] = tmp2[0]
    dst_a[2] = tmp1[1]
    dst_a[3] = tmp2[1]
    dst_a[4] = tmp1[2]
    dst_a[5] = tmp2[2]
    dst_a[6] = tmp1[3]
    dst_a[7] = tmp2[3]
    dst_a[8] = tmp1[4]
    dst_a[9] = tmp2[4]
    dst_a[10] = tmp1[5]
    dst_a[11] = tmp2[5]
    dst_a[12] = tmp1[6]
    dst_a[13] = tmp2[6]
    dst_a[14] = tmp1[7]
    dst_a[15] = tmp2[7]
    return dst

def mm_unpacklo_pd(a: M128D, b: M128D) -> M128D:
    dst = a.get_doubles().copy()
    dst[1] = b.get_first()
    return M128D(dst)

def mm_xor_pd(a: M128D, b: M128D) -> M128D:
    return M128D((a.as_uints() ^ b.as_uints()).view(np.float64))

def mm_xor_si128(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i64() ^ b.get_i64())

# =================== M64 ===================
# Not supported functions of the packed M64 type

def mm_add_si64(a: M64, b: M64) -> M64:
    pass
def mm_cvtpd_pi32(a: M128D) -> M64:
    pass
def mm_cvtpi32_pd(a: M64) -> M128D:
    pass
def mm_movepi64_pi64(a: M128I) -> M64:
    pass
def mm_movpi64_epi64(a: M64) -> M128I:
    pass
def mm_mul_su32(a: M64, b: M64) -> M64:
    pass
def mm_set_epi64(e1: M64, e0: M64) -> M128I:
    pass
def mm_set1_epi64(a: M64) -> M128I:
    pass
def mm_sub_si64(a: M64, b: M64) -> M64:
    pass
