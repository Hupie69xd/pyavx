import SSE
import SSE2
import SSE3
import SSSE3
import SSE4_1
##import SSE4_2

from SSE import Lane, M128, type_format
from SSE2 import M128D, M128I, mm_undefined_si128
# from SSE4_1 import vector2mask
from utility_functions import *

import numpy as np

class _M256Base(Lane):
    def __init__(self, lanes):
        self._lanes = lanes
    def get_lanes(self):
        return self._lanes

class M256I(_M256Base):
    def __init__(self, lanes):
        super().__init__(lanes)

class M256D(_M256Base):
    def __init__(self, lanes):
        super().__init__(lanes)

class M256(_M256Base):
    def __init__(self, lanes):
        super().__init__(lanes)


def read_funcs(filename: str) -> [str]:
    # returns list of function names
    with open(filename, 'r', encoding='utf-8') as f:
        data = f.read()

    out = []
    for i in data.split('\n'):
        e = i.find('(')
        s = i.find(" _")
        out.append(i[s+2:e].strip())
    return out

def make_list():
    def repl(l):
        return [i.replace("mm256_", "_mm_") for i in l]
    s1 = repl(read_funcs("txt/SSE_funcs.txt"))
    s2 = repl(read_funcs("txt/SSE2_funcs.txt"))
    s3 = repl(read_funcs("txt/SSE3_funcs.txt"))
    ss3 = repl(read_funcs("txt/SSSE3_funcs.txt"))
    s41 = repl(read_funcs("txt/SSE4_1_funcs.txt"))
    s42 = repl(read_funcs("txt/SSE4_1_funcs.txt"))
    return [('SSE',   s1) , ('SSE2',   s2) , ('SSE3'  , s3),
            ('SSSE3', ss3), ('SSE4_1', s41), ('SSE4_2', s42), ]

##st = make_list()
##write_funcs("AVX_py.txt", [convert_avx_func(i, find_func(st, i)) for i in extract_funcs("AVX.txt")])

def find_func(list_, f) -> str:
    ## mangle arg
    f = f[f.find(" _mm") : f.find('(')].strip()
    f = f.strip().replace("_mm256", "mm")
    ## search
    for n, l in list_:
        for i in l:
            if f == i:
                return n
    return ""

def arg_names(args):
    if args == '' or args == "void":
        return ''
    args = args.split(',')
    output = []
    for i in args:
        t, name = i.strip().rsplit(' ', maxsplit=1)
        if t.startswith("__"):
            t = t[2:].title()
        output.append(name)
    return ", ".join(output)

_TYPE_TABLE = {"void": "None", "char": "int", "unsigned int": "int", "void*": "(bytes, int)",
               "float": "float", "double": "float", "short": 'int', "long long": "int",
               "unsigned __int64": 'int', "const int": "int", "__m256": "M256",
               "__m128": "M128", "__m256i": "M256I", "__m256d": "M256D",
               "__m128i": "M128I", "__m128d": "M128D", "__int32": "int",
               "__int64": "int", "int":"int"}
def convert_avx_func(func, found=""):
    p = func.find(" _mm")
    p2 = func.find('(')
    ret_type = _TYPE_TABLE[func[ : p].strip()]
    func_name = func[p+2 : p2].strip()
    arg = func[p2+1: func.find(')')]
    args = type_format(arg)
    arg_names_ = arg_names(arg)
    dec = f"def {func_name}({args}) -> {ret_type}:"
    sse_func = func_name.replace("mm256", "mm")
    if found:
        definition = f"\n    return {ret_type}(use_sse_def({found}.{sse_func}, {arg_names_}))"
    else:
        definition = ""
    return dec + definition


### helper functions

def use_sse_def(func, a, *args):
    output = []
    if isinstance(args[0], _M256Base):
        b = args[0]
        args = args[1:]
        for x, y in zip(a.get_lanes(), b.get_lanes()):
            output.append(func(x, y, *args))
    else:
        for x in a.get_lanes():
            output.append(func(x, *args))
    return output


### Intrinsics
def mm256_add_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_add_pd, a, b))

def mm256_add_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_add_ps, a, b))

def mm256_addsub_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE3.mm_addsub_pd, a, b))

def mm256_addsub_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE3.mm_addsub_ps, a, b))

def mm256_and_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_and_pd, a, b))

def mm256_and_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_and_ps, a, b))

def mm256_andnot_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_andnot_pd, a, b))

def mm256_andnot_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_andnot_ps, a, b))

def mm256_blend_pd(a: M256D, b: M256D, imm8: int) -> M256D:
    return M256D(use_sse_def(SSE4_1.mm_blend_pd, a, b, imm8))

def mm256_blend_ps(a: M256, b: M256, imm8: int) -> M256:
    return M256(use_sse_def(SSE4_1.mm_blend_ps, a, b, imm8))

def mm256_blendv_pd(a: M256D, b: M256D, mask: M256D) -> M256D:
    return M256D(use_sse_def(SSE4_1.mm_blendv_pd, a, b, mask)) # XXX: Fix

def mm256_blendv_ps(a: M256, b: M256, mask: M256) -> M256:
    return M256(use_sse_def(SSE4_1.mm_blendv_ps, a, b, mask)) # XXX: Fix

def mm256_broadcast_pd(mem_addr: (bytes, int)) -> M256D:
    tmp = SSE2.mm_load_pd(mem_addr)
    return M256D(tmp, tmp.copy())
    
def mm256_broadcast_ps(mem_addr: (bytes, int)) -> M256:
    tmp = SSE.mm_load_ps(mem_addr)
    return M256(tmp, tmp.copy())

def mm256_broadcast_sd(mem_addr: (bytes, int)) -> M256D:
    tmp = SSE2.mm_load_pd1(mem_addr)
    return M256D([tmp, tmp.copy()])

def mm_broadcast_ss(mem_addr: (bytes, int)) -> M128:
    return SSE.mm_load_ps1(mem_addr)

def mm256_broadcast_ss(mem_addr: (bytes, int)) -> M256:
    tmp = mm_broadcast_ss(mem_addr)
    return M256([tmp, tmp.copy()])

def mm256_castpd_ps(a: M256D) -> M256:
    return M256(use_sse_def(SSE2.mm_castpd_ps, a))

def mm256_castpd_si256(a: M256D) -> M256I:
    return M256I(use_sse_def(SSE2.mm_castpd_si128, a))

def mm256_castpd128_pd256(a: M128D) -> M256D:
    tmp = SSE2.mm_undefined_pd()
    return M256D([a.copy(), tmp])

def mm256_castpd256_pd128(a: M256D) -> M128D:
    return a.get_lanes()[0].copy()

def mm256_castps_pd(a: M256) -> M256D:
    return M256D(use_sse_def(SSE2.mm_castps_pd, a))

def mm256_castps_si256(a: M256) -> M256I:
    return M256I(use_sse_def(SSE2.mm_castps_si128, a))

def mm256_castps128_ps256(a: M128) -> M256:
    tmp = SSE.mm_undefined_ps()
    return M256([a.copy(), tmp])

def mm256_castps256_ps128(a: M256) -> M128:
    return a.get_lanes()[0].copy()

def mm256_castsi128_si256(a: M128I) -> M256I:
    tmp = SSE2.mm_undefined_si128()
    return M256I([a.copy(), tmp])

def mm256_castsi256_pd(a: M256I) -> M256D:
    return M256D(use_sse_def(SSE2.mm_castsi128_pd, a))

def mm256_castsi256_ps(a: M256I) -> M256:
    return M256(use_sse_def(SSE2.mm_castsi128_ps, a))

def mm256_castsi256_si128(a: M256I) -> M128I:
    return a.get_lanes()[0].copy()

def mm256_ceil_pd(a: M256D) -> M256D:
    return M256D(use_sse_def(SSE4_1.mm_ceil_pd, a))

def mm256_ceil_ps(a: M256) -> M256:
    return M256(use_sse_def(SSE4_1.mm_ceil_ps, a))

def mm_cmp_pd(a: M128D, b: M128D, imm8: int) -> M128D:
    pass # XXX: 'spaceship' function

def mm256_cmp_pd(a: M256D, b: M256D, imm8: int) -> M256D:
    pass # XXX: 'spaceship' function

def mm_cmp_ps(a: M128, b: M128, imm8: int) -> M128:
    pass # XXX: 'spaceship' function

def mm256_cmp_ps(a: M256, b: M256, imm8: int) -> M256:
    pass # XXX: 'spaceship' function

def mm_cmp_sd(a: M128D, b: M128D, imm8: int) -> M128D:
    pass # XXX: 'spaceship' function

def mm_cmp_ss(a: M128, b: M128, imm8: int) -> M128:
    pass # XXX: 'spaceship' function

def mm256_cvtepi32_pd(a: M128I) -> M256D:
    return M256D(use_sse_def(SSE2.mm_cvtepi32_pd, a))

def mm256_cvtepi32_ps(a: M256I) -> M256:
    return M256(use_sse_def(SSE2.mm_cvtepi32_ps, a))

def mm256_cvtpd_epi32(a: M256D) -> M128I:
    return M128I(use_sse_def(SSE2.mm_cvtpd_epi32, a))

def mm256_cvtpd_ps(a: M256D) -> M128:
    return M128(use_sse_def(SSE2.mm_cvtpd_ps, a))

def mm256_cvtps_epi32(a: M256) -> M256I:
    return M256I(use_sse_def(SSE2.mm_cvtps_epi32, a))

def mm256_cvtps_pd(a: M128) -> M256D:
    return M256D(use_sse_def(SSE2.mm_cvtps_pd, a))

def mm256_cvtsd_f64(a: M256D) -> float:
    return a.get_lanes()[0].get_floats()[0]

def mm256_cvtsi256_si32(a: M256I) -> int:
    return a.get_lanes()[0].get_i32()[0]

def mm256_cvtss_f32(a: M256) -> float:
    return a.get_lanes()[0].get_floats()[0]

def mm256_cvttpd_epi32(a: M256D) -> M128I:
    tmp = a.get_lanes()
    dst = np.concatenate((tmp[0].get_doubles(), tmp[1].get_doubles()))
    return M128I(dst.astype(np.int32))

def mm256_cvttps_epi32(a: M256) -> M256I:
    return M256I(use_sse_def(SSE2.mm_cvttps_epi32, a))

def mm256_div_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_div_pd, a, b))

def mm256_div_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_div_ps, a, b))

def mm256_dp_ps(a: M256, b: M256, imm8: int) -> M256:
    return M256(use_sse_def(SSE4_1.mm_dp_ps, a, b, imm8))  # XXX: test this

def mm256_extract_epi32(a: M256I, index: int) -> int:
    tmp = a.get_lanes()
    if index < 4:
        return tmp[0].get_i32()[index]
    else:
        return tmp[1].get_i32()[index - 4]

def mm256_extract_epi64(a: M256I, index: int) -> int:
    tmp = a.get_lanes()
    if index < 2:
        return tmp[0].get_i64()[index]
    else:
        return tmp[1].get_i64()[index - 2]

def mm256_extractf128_pd(a: M256D, imm8: int) -> M128D:
    tmp = a.get_lanes()
    return tmp[imm8].copy()

def mm256_extractf128_ps(a: M256, imm8: int) -> M128:
    tmp = a.get_lanes()
    return tmp[imm8].copy()

def mm256_extractf128_si256(a: M256I, imm8: int) -> M128I:
    tmp = a.get_lanes()
    return tmp[imm8].copy()

def mm256_floor_pd(a: M256D) -> M256D:
    return M256D(use_sse_def(SSE4_1.mm_floor_pd, a))

def mm256_floor_ps(a: M256) -> M256:
    return M256(use_sse_def(SSE4_1.mm_floor_ps, a))

def mm256_hadd_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE3.mm_hadd_pd, a, b))

def mm256_hadd_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE3.mm_hadd_ps, a, b))

def mm256_hsub_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE3.mm_hsub_pd, a, b))

def mm256_hsub_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE3.mm_hsub_ps, a, b))

def mm256_insert_epi16(a: M256I, i: int, index: int) -> M256I:
    tmp = a.get_lanes()
    if index < 8:
        lanes = [SSE2.mm_insert_epi16(tmp[0], i, index), tmp[1].copy()]
    else:
        lanes = [tmp[0].copy(), SSE2.mm_insert_epi16(tmp[1], i, index - 8)]
    return M256I(lanes)

def mm256_insert_epi32(a: M256I, i: int, index: int) -> M256I:
    tmp = a.get_lanes()
    if index < 4:
        lanes = [SSE2.mm_insert_epi16(tmp[0], i, index), tmp[1].copy()]
    else:
        lanes = [tmp[0].copy(), SSE2.mm_insert_epi16(tmp[1], i, index - 4)]
    return M256I(lanes)

def mm256_insert_epi64(a: M256I, i: int, index: int) -> M256I:
    tmp = a.get_lanes()
    if index < 2:
        lanes = [SSE2.mm_insert_epi16(tmp[0], i, index), tmp[1].copy()]
    else:
        lanes = [tmp[0].copy(), SSE2.mm_insert_epi16(tmp[1], i, index - 2)]
    return M256I(lanes)

def mm256_insert_epi8(a: M256I, i: int, index: int) -> M256I:
    tmp = a.get_lanes()
    if index < 16:
        lanes = [SSE2.mm_insert_epi16(tmp[0], i, index), tmp[1].copy()]
    else:
        lanes = [tmp[0].copy(), SSE2.mm_insert_epi16(tmp[1], i, index - 16)]
    return M256I(lanes)

def mm256_insertf128_pd(a: M256D, b: M128D, imm8: int) -> M256D:
    return M256D(insert_128(a, b, imm8))

def mm256_insertf128_ps(a: M256, b: M128, imm8: int) -> M256:
    return M256(insert_128(a, b, imm8))

def mm256_insertf128_si256(a: M256I, b: M128I, imm8: int) -> M256I:
    return M256I(insert_128(a, b, imm8))

def mm256_lddqu_si256(mem_addr: (bytes, int)) -> M256I:
    return mm256_load_si256(mem_addr)

def mm256_load_pd(mem_addr: (bytes, int)) -> M256D:
    data, start = mem_addr
    l0 = SSE2.mm_load_pd(mem_addr)
    l1 = SSE2.mm_load_pd((data, start + 16))
    return M256D([l0, l1])

def mm256_load_ps(mem_addr: (bytes, int)) -> M256:
    data, start = mem_addr
    l0 = SSE.mm_load_ps(mem_addr)
    l1 = SSE.mm_load_ps((data, start + 16))
    return M256([l0, l1])

def mm256_load_si256(mem_addr: (bytes, int)) -> M256I:
    data, start = mem_addr
    l0 = SSE2.mm_load_si128(mem_addr)
    l1 = SSE2.mm_load_si128((data, start + 16))
    return mm256_set_m128i(l1, l0)

def mm256_loadu_pd(mem_addr: (bytes, int)) -> M256D:
    return mm256_load_pd(mem_addr)

def mm256_loadu_ps(mem_addr: (bytes, int)) -> M256:
    return mm256_load_ps(mem_addr)

def mm256_loadu_si256(mem_addr: (bytes, int)) -> M256I:
    return mm256_load_si256(mem_addr)

def mm256_loadu2_m128(hiaddr: (bytes, int), loaddr: (bytes, int)) -> M256:
    l0 = SSE.mm_load_ps(hiaddr)
    l1 = SSE.mm_load_ps(loaddr)
    return mm256_set_m128(l1, l0)

def mm256_loadu2_m128d(hiaddr: (bytes, int), loaddr: (bytes, int)) -> M256D:
    l0 = SSE2.mm_load_pd(hiaddr)
    l1 = SSE2.mm_load_pd(loaddr)
    return mm256_set_m128d(l1, l0)

def mm256_loadu2_m128i(hiaddr: (bytes, int), loaddr: (bytes, int)) -> M256I:
    l0 = SSE2.mm_load_si128(hiaddr)
    l1 = SSE2.mm_load_si128(loaddr)
    return M256I([l0, l1])

def mm_maskload_pd(mem_addr: (bytes, int), mask: M128I) -> M128D:
    i_mask = vector2mask(mask.get_u64())
    data, start = mem_addr
    dst = SSE2.mm_undefined_pd()
    dst_a = dst.get_doubles()
    for n, in range(2):
        if i_mask & 1:
            dst_a[n] = SSE2.read_double((data, start+8*n))
        else:
            dst_a[n] = 0.0
        i_mask = i_mask >> 1
    return dst

def mm256_maskload_pd(mem_addr: (bytes, int), mask: M256I) -> M256D:
    data, start = mem_addr
    tmp = mask.get_lanes()
    l0 = mm_maskload_pd((data, start+0), tmp[0])
    l1 = mm_maskload_pd((data, start+16), tmp[1])
    return M256D([l0, l1])

def mm_maskload_ps(mem_addr: (bytes, int), mask: M128I) -> M128:
    i_mask = vector2mask(mask.get_u32())
    data, start = mem_addr
    dst = SSE.mm_undefined_ps()
    dst_a = dst.get_floats()
    for n, in range(4):
        if i_mask & 1:
            dst_a[n] = SSE.read_float((data, start+4*n))
        else:
            dst_a[n] = 0.0
        i_mask = i_mask >> 1
    return M128(masked_load_pd(mem_addr, i_mask))

def mm256_maskload_ps(mem_addr: (bytes, int), mask: M256I) -> M256:
    data, start = mem_addr
    tmp = mask.get_lanes()
    l0 = mm_maskload_ps((data, start+0), tmp[0])
    l1 = mm_maskload_ps((data, start+16), tmp[1])
    return mm256_set_m128(l1, l0)

def mm_maskstore_pd(mem_addr: (bytes, int), mask: M128I, a: M128D) -> None:
    i_mask = vector2mask(mask.get_u64())
    data, start = mem_addr
    for n, i in enumerate(a.get_doubles()):
        if i_mask & 1:
            SSE2.write_doubles((data, start+8*n), [i])
        i_mask = i_mask >> 1

def mm256_maskstore_pd(mem_addr: (bytes, int), mask: M256I, a: M256D) -> None:
    data, start = mem_addr
    tmp_mask = mask.get_lanes()
    tmp = a.get_lanes()
    mm_maskstore_pd((data, start+0), tmp_mask[0], tmp[0])
    mm_maskstore_pd((data, start+16), tmp_mask[1], tmp[1])

def mm_maskstore_ps(mem_addr: (bytes, int), mask: M128I, a: M128) -> None:
    i_mask = vector2mask(mask.get_u32())
    data, start = mem_addr
    for n, i in enumerate(a.get_floats()):
        if i_mask & 1:
            SSE.write_floats((data, start+8*n), [i])
        i_mask = i_mask >> 1

def mm256_maskstore_ps(mem_addr: (bytes, int), mask: M256I, a: M256) -> None:
    data, start = mem_addr
    tmp_mask = mask.get_lanes()
    tmp = a.get_lanes()
    mm_maskstore_ps((data, start+0), tmp_mask[0], tmp[0])
    mm_maskstore_ps((data, start+16), tmp_mask[1], tmp[1])

def mm256_max_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_max_pd, a, b))

def mm256_max_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_max_ps, a, b))

def mm256_min_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_min_pd, a, b))

def mm256_min_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_min_ps, a, b))

def mm256_movedup_pd(a: M256D) -> M256D:
    return M256D(use_sse_def(SSE3.mm_movedup_pd, a))

def mm256_movehdup_ps(a: M256) -> M256:
    return M256(use_sse_def(SSE3.mm_movehdup_ps, a))

def mm256_moveldup_ps(a: M256) -> M256:
    return M256(use_sse_def(SSE3.mm_moveldup_ps, a))

def mm256_movemask_pd(a: M256D) -> int:
    ml, mh = use_sse_def(SSE2.mm_movemask_pd, a)
    return ml | (mh << 2)

def mm256_movemask_ps(a: M256) -> int:
    ml, mh = use_sse_def(SSE.mm_movemask_ps, a)
    return ml | (mh << 4)

def mm256_mul_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_mul_pd, a, b))

def mm256_mul_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_mul_ps, a, b))

def mm256_or_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_or_pd, a, b))

def mm256_or_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_or_ps, a, b))

def mm_permute_pd(a: M128D, imm8: int) -> M128D:
    tmp = a.get_doubles().copy()
    if imm8 == 1:
        tmp[0] = tmp[1]
        return M128D(tmp)
    else:
        tmp[1] = tmp[0]
        return M128D(tmp)

def mm256_permute_pd(a: M256D, imm8: int) -> M256D:
    return M256D(use_sse_def(mm256_permute_pd, a, imm8))

def mm_permute_ps(a: M128, imm8: int) -> M128:
    dst = a.get_floats().copy()
    tmp = a.get_floats()
    control = imm8
    for i in range(4):
        dst[i] = tmp[control & 3]
        control = control >> 2
    return M128(dst)

def mm256_permute_ps(a: M256, imm8: int) -> M256:
    return M256(use_sse_def(mm_permute_ps, a, imm8))

def mm256_permute2f128_pd(a: M256D, b: M256D, imm8: int) -> M256D:
    return M256D(permute256(a, b, imm8, SSE2.mm_setzero_pd))

def mm256_permute2f128_ps(a: M256, b: M256, imm8: int) -> M256:
    return M256(permute256(a, b, imm8, SSE.mm_setzero_ps))

def mm256_permute2f128_si256(a: M256I, b: M256I, imm8: int) -> M256I:
    return M256I(permute256(a, b, imm8, SSE2.mm_setzero_si128))

def mm_permutevar_pd(a: M128D, b: M128I) -> M128D:
    dst = a.get_doubles().copy()
    tmp = a.get_doubles()
    control = b.get_u64()
    for i in range(2):
        dst[i] = tmp[1] if (control[i] & 1) else tmp[0]
    return M128D(dst)

def mm256_permutevar_pd(a: M256D, b: M256I) -> M256D:
    return M256D(use_sse_def(mm_permutevar_pd, a, b))

def mm_permutevar_ps(a: M128, b: M128I) -> M128:
    dst = a.get_floats().copy()
    tmp = a.get_floats()
    control = b.get_u32()
    for i in range(4):
        dst[i] = tmp[control[i] & 3]
    return M128(dst)

def mm256_permutevar_ps(a: M256, b: M256I) -> M256:
    return M256(use_sse_def(mm_permutevar_ps, a, b))

def mm256_rcp_ps(a: M256) -> M256:
    return M256(use_sse_def(SSE.mm_rcp_ps, a))

def mm256_round_pd(a: M256D, rounding: int) -> M256D:
    return M256D(use_sse_def(SSE4_1.mm_round_pd, a, rounding))

def mm256_round_ps(a: M256, rounding: int) -> M256:
    return M256(use_sse_def(SSE4_1.mm_round_ps, a, rounding))

def mm256_rsqrt_ps(a: M256) -> M256:
    return M256(use_sse_def(SSE.mm_rsqrt_ps, a))

def mm256_set_epi16(e15: int, e14: int, e13: int, e12: int, e11: int, e10: int, e9: int, e8: int, e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M256I:
    l0 = SSE2.mm_set_epi16(e7, e6, e5, e4, e3, e2, e1, e0)
    l1 = SSE2.mm_set_epi16(e15, e14, e13, e12, e11, e10, e9, e8)
    return M256I([l0, l1])

def mm256_set_epi32(e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M256I:
    l0 = SSE2.mm_set_epi32(e3, e2, e1, e0)
    l1 = SSE2.mm_set_epi32(e7, e6, e5, e4)
    return M256I([l0, l1])

def mm256_set_epi64x(e3: int, e2: int, e1: int, e0: int) -> M256I:
    l0 = SSE2.mm_set_epi64x(e1, e0)
    l1 = SSE2.mm_set_epi64x(e3, e2)
    return M256I([l0, l1])

def mm256_set_epi8(e31: int, e30: int, e29: int, e28: int, e27: int, e26: int, e25: int, e24: int, e23: int, e22: int, e21: int, e20: int, e19: int, e18: int, e17: int, e16: int, e15: int, e14: int, e13: int, e12: int, e11: int, e10: int, e9: int, e8: int, e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M256I:
    l0 = SSE2.mm_set_epi8(e15, e14, e13, e12, e11, e10, e9, e8,
                          e7,  e6,  e5,  e4,  e3,  e2,  e1, e0)
    l1 = SSE2.mm_set_epi8(e31, e30, e29, e28, e27, e26, e25, e24,
                          e23, e22, e21, e20, e19, e18, e17, e16)
    return M256I([l0, l1])

def mm256_set_m128(hi: M128, lo: M128) -> M256:
    return M256([lo, hi])

def mm256_set_m128d(hi: M128D, lo: M128D) -> M256D:
    return M256D([lo, hi])

def mm256_set_m128i(hi: M128I, lo: M128I) -> M256I:
    return M256I([lo, hi])

def mm256_set_pd(e3: float, e2: float, e1: float, e0: float) -> M256D:
    l0 = SSE2.mm_set_pd(e1, e0)
    l1 = SSE2.mm_set_pd(e3, e2)
    return M256D([l0, l1])

def mm256_set_ps(e7: float, e6: float, e5: float, e4: float, e3: float, e2: float, e1: float, e0: float) -> M256:
    l0 = SSE.mm_set_ps(e3, e2, e1, e0)
    l1 = SSE.mm_set_ps(e7, e6, e5, e4)
    return M256([l0, l1])

def mm256_set1_epi16(a: int) -> M256I:
    l0 = SSE2.mm_set1_epi16(a)
    l1 = SSE2.mm_set1_epi16(a)
    return M256I([l0, l1])

def mm256_set1_epi32(a: int) -> M256I:
    l0 = SSE2.mm_set1_epi32(a)
    l1 = SSE2.mm_set1_epi32(a)
    return M256I([l0, l1])

def mm256_set1_epi64x(a: int) -> M256I:
    l0 = SSE2.mm_set1_epi64x(a)
    l1 = SSE2.mm_set1_epi64x(a)
    return M256I([l0, l1])

def mm256_set1_epi8(a: int) -> M256I:
    l0 = SSE2.mm_set1_epi8(a)
    l1 = SSE2.mm_set1_epi8(a)
    return M256I([l0, l1])

def mm256_set1_pd(a: float) -> M256D:
    l0 = SSE2.mm_set1_pd(a)
    l1 = SSE2.mm_set1_pd(a)
    return M256D([l0, l1])

def mm256_set1_ps(a: float) -> M256:
    l0 = SSE.mm_set1_ps(a)
    l1 = SSE.mm_set1_ps(a)
    return M256([l0, l1])

def mm256_setr_epi16(e15: int, e14: int, e13: int, e12: int, e11: int, e10: int, e9: int, e8: int, e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M256I:
    return mm256_set_epi16(e0, e1, e2,  e3,  e4,  e5,  e6,  e7,
                           e8, e9, e10, e11, e12, e13, e14, e15)

def mm256_setr_epi32(e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M256I:
    return mm256_set_epi32(e0, e1, e2, e3, e4, e5, e6, e7)

def mm256_setr_epi64x(e3: int, e2: int, e1: int, e0: int) -> M256I:
    return mm256_set_epi64x(e0, e1, e2, e3)

def mm256_setr_epi8(e31: int, e30: int, e29: int, e28: int, e27: int, e26: int, e25: int, e24: int, e23: int, e22: int, e21: int, e20: int, e19: int, e18: int, e17: int, e16: int, e15: int, e14: int, e13: int, e12: int, e11: int, e10: int, e9: int, e8: int, e7: int, e6: int, e5: int, e4: int, e3: int, e2: int, e1: int, e0: int) -> M256I:
    return mm256_set_epi8(e0,  e1,  e2,  e3,  e4,  e5,  e6,  e7,
                          e8,  e9,  e10, e11, e12, e13, e14, e15,
                          e16, e17, e18, e19, e20, e21, e22, e23,
                          e24, e25, e26, e27, e28, e29, e30, e31)

def mm256_setr_m128(lo: M128, hi: M128) -> M256:
    return mm256_set_m128(hi, lo)

def mm256_setr_m128d(lo: M128D, hi: M128D) -> M256D:
    return mm256_set_m128d(hi, lo)

def mm256_setr_m128i(lo: M128I, hi: M128I) -> M256I:
    return mm256_set_m128i(hi, lo)

def mm256_setr_pd(e3: float, e2: float, e1: float, e0: float) -> M256D:
    return mm256_set_pd(e0, e1, e2, e3)

def mm256_setr_ps(e7: float, e6: float, e5: float, e4: float, e3: float, e2: float, e1: float, e0: float) -> M256:
    return mm256_set_ps(e0, e1, e2, e3, e4, e5, e6, e7)

def mm256_setzero_pd() -> M256D:
    return M256D([SSE2.mm_setzero_pd(), SSE2.mm_setzero_pd()])

def mm256_setzero_ps() -> M256:
    return M256([SSE.mm_setzero_ps(), SSE.mm_setzero_ps()])

def mm256_setzero_si256() -> M256I:
    return M256I([SSE2.mm_setzero_si128(), SSE2.mm_setzero_si128()])

def mm256_shuffle_pd(a: M256D, b: M256D, imm8: int) -> M256D:
    return M256D(use_sse_def(SSE2.mm_shuffle_pd, a, b, imm8))

def mm256_shuffle_ps(a: M256, b: M256, imm8: int) -> M256:
    return M256(use_sse_def(SSE.mm_shuffle_ps, a, b, imm8))

def mm256_sqrt_pd(a: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_sqrt_pd, a))

def mm256_sqrt_ps(a: M256) -> M256:
    return M256(use_sse_def(SSE.mm_sqrt_ps, a))

def mm256_store_pd(mem_addr: (bytes, int), a: M256D) -> None:
    data, start = mem_addr
    return mm256_storeu2_m128d((data, start + 16), mem_addr, a)

def mm256_store_ps(mem_addr: (bytes, int), a: M256) -> None:
    data, start = mem_addr
    return mm256_storeu2_m128((data, start + 16), mem_addr, a)

def mm256_store_si256(mem_addr: (bytes, int), a: M256I) -> None:
    data, start = mem_addr
    return mm256_storeu2_m128i((data, start + 16), mem_addr, a)

def mm256_storeu_pd(mem_addr: (bytes, int), a: M256D) -> None:
    return mm256_store_pd(mem_addr, a)

def mm256_storeu_ps(mem_addr: (bytes, int), a: M256) -> None:
    return mm256_store_ps(mem_addr, a)

def mm256_storeu_si256(mem_addr: (bytes, int), a: M256I) -> None:
    return mm256_store_si256(mem_addr, a)

def mm256_storeu2_m128(hiaddr: (bytes, int), loaddr: (bytes, int), a: M256) -> None:
    l0, l1 = a.get_lanes()
    SSE.mm_store_ps(loaddr, l0)
    SSE.mm_store_ps(hiaddr, l1)

def mm256_storeu2_m128d(hiaddr: (bytes, int), loaddr: (bytes, int), a: M256D) -> None:
    l0, l1 = a.get_lanes()
    SSE2.mm_store_pd(loaddr, l0)
    SSE2.mm_store_pd(hiaddr, l1)

def mm256_storeu2_m128i(hiaddr: (bytes, int), loaddr: (bytes, int), a: M256I) -> None:
    l0, l1 = a.get_lanes()
    SSE2.mm_store_si128(loaddr, l0)
    SSE2.mm_store_si128(hiaddr, l1)

def mm256_stream_pd(mem_addr: (bytes, int), a: M256D) -> None:
    return mm256_store_pd(mem_addr, a)

def mm256_stream_ps(mem_addr: (bytes, int), a: M256) -> None:
    return mm256_store_ps(mem_addr, a)

def mm256_stream_si256(mem_addr: (bytes, int), a: M256I) -> None:
    return mm256_store_si256(mem_addr, a)

def mm256_sub_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_sub_pd, a, b))

def mm256_sub_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_sub_ps, a, b))

def mm_testc_pd(a: M128D, b: M128D) -> int:
    _, cf = test_signs(a.as_uints(64), b.as_uints(64))
    return cf

def mm256_testc_pd(a: M256D, b: M256D) -> int:
    tmp1 = a.get_lanes()
    tmp2 = b.get_lanes()
    return mm_testc_pd(tmp1[0], tmp2[0]) & mm_testc_pd(tmp1[1], tmp2[1])

def mm_testc_ps(a: M128, b: M128) -> int:
    _, cf = test_signs(a.as_uints(32), b.as_uints(32))
    return cf

def mm256_testc_ps(a: M256, b: M256) -> int:
    tmp1 = a.get_lanes()
    tmp2 = b.get_lanes()
    return mm_testc_ps(tmp1[0], tmp2[0]) & mm_testc_ps(tmp1[1], tmp2[1])

def mm256_testc_si256(a: M256I, b: M256I) -> int:
    tmp1 = a.get_lanes()
    tmp2 = b.get_lanes()
    return SSE4_1.mm_testc_si128(tmp1[0], tmp2[0]) & \
           SSE4_1.mm_testc_si128(tmp1[1], tmp2[1])

def mm_testnzc_pd(a: M128D, b: M128D) -> int:
    zf, cf = test_signs(a.as_uints(64), b.as_uints(64))
    return 1 if zf == cf == 0 else 0

def mm256_testnzc_pd(a: M256D, b: M256D) -> int:
    tmp1 = a.get_lanes()
    tmp2 = b.get_lanes()
    return mm_testnzc_pd(tmp1[0], tmp2[0]) & mm_testnzc_pd(tmp1[1], tmp2[1])

def mm_testnzc_ps(a: M128, b: M128) -> int:
    zf, cf = test_signs(a.as_uints(64), b.as_uints(64))
    return 1 if zf == cf == 0 else 0

def mm256_testnzc_ps(a: M256, b: M256) -> int:
    tmp1 = a.get_lanes()
    tmp2 = b.get_lanes()
    return mm_testnzc_ps(tmp1[0], tmp2[0]) & mm_testnzc_ps(tmp1[1], tmp2[1])

def mm256_testnzc_si256(a: M256I, b: M256I) -> int:
    tmp1 = a.get_lanes()
    tmp2 = b.get_lanes()
    return SSE4_1.mm_testnzc_si128(tmp1[0], tmp2[0]) & \
           SSE4_1.mm_testnzc_si128(tmp1[1], tmp2[1])

def mm_testz_pd(a: M128D, b: M128D) -> int:
    zf, _ = test_signs(a.as_uints(64), b.as_uints(64))
    return zf

def mm256_testz_pd(a: M256D, b: M256D) -> int:
    tmp1 = a.get_lanes()
    tmp2 = b.get_lanes()
    return mm_testz_pd(tmp1[0], tmp2[0]) & mm_testz_pd(tmp1[1], tmp2[1])

def mm_testz_ps(a: M128, b: M128) -> int:
    zf, _ = test_signs(a.as_uints(32), b.as_uints(32))
    return zf

def mm256_testz_ps(a: M256, b: M256) -> int:
    tmp1 = a.get_lanes()
    tmp2 = b.get_lanes()
    return mm_testz_ps(tmp1[0], tmp2[0]) & mm_testz_ps(tmp1[1], tmp2[1])

def mm256_testz_si256(a: M256I, b: M256I) -> int:
    x, y = use_sse_def(SSE4_1.mm_testz_si128, a, b)
    return x & y

def mm256_undefined_pd() -> M256D:
    return M256D([SSE2.mm_undefined_pd(), SSE2.mm_undefined_pd()])

def mm256_undefined_ps() -> M256:
    return M256([SSE.mm_undefined_ps(), SSE.mm_undefined_ps()])

def mm256_undefined_si256() -> M256I:
    return M256I([SSE2.mm_undefined_si128(), SSE2.mm_undefined_si128()])

def mm256_unpackhi_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_unpackhi_pd, a, b))

def mm256_unpackhi_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_unpackhi_ps, a, b))

def mm256_unpacklo_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_unpacklo_pd, a, b))

def mm256_unpacklo_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_unpacklo_ps, a, b))

def mm256_xor_pd(a: M256D, b: M256D) -> M256D:
    return M256D(use_sse_def(SSE2.mm_xor_pd, a, b))

def mm256_xor_ps(a: M256, b: M256) -> M256:
    return M256(use_sse_def(SSE.mm_xor_ps, a, b))

def mm256_zeroall() -> None:
    raise NotImplementedError("¯\_ツ_/¯")

def mm256_zeroupper() -> None:
    raise NotImplementedError("¯\_ツ_/¯")

def mm256_zextpd128_pd256(a: M128D) -> M256D:
    hi = SSE2.mm_setzero_pd()
    return M256D([a.copy(), hi])

def mm256_zextps128_ps256(a: M128) -> M256:
    hi = SSE.mm_setzero_ps()
    return M256([a.copy(), hi])

def mm256_zextsi128_si256(a: M128I) -> M256I:
    hi = SSE2.mm_setzero_si128()
    return M256I([a.copy(), hi])

