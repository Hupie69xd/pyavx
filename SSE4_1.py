import numpy as np

from SSE import M128, bools_to_mask, mm_setr_ps, vector2mask
from SSE2 import M128D, M128I, mm_load_si128, mm_setzero_si128, mm_setr_pd, mm_set_epi64x
from utility_functions import blend, round_to, saturation, test_zf_cf


### Intrinsics
def mm_blend_epi16(a: M128I, b: M128I, imm8: int) -> M128I:
    return M128I(blend(a.get_i16(), b.get_i16(), imm8))

def mm_blend_pd(a: M128D, b: M128D, imm8: int) -> M128D:
    return M128D(blend(a.get_doubles(), b.get_doubles(), imm8))

def mm_blend_ps(a: M128, b: M128, imm8: int) -> M128:
    return M128(blend(a.get_floats(), b.get_floats(), imm8))

def mm_blendv_epi8(a: M128I, b: M128I, mask: M128I) -> M128I:
    imm8 = vector2mask(mask.get_u8())
    return M128I(blend(a.get_i8(), b.get_i8(), imm8))

def mm_blendv_pd(a: M128D, b: M128D, mask: M128D) -> M128D:
    imm8 = vector2mask(mask.get_doubles().view(np.uint64))
    return M128D(blend(a.get_doubles(), b.get_doubles(), imm8))

def mm_blendv_ps(a: M128, b: M128, mask: M128) -> M128:
    imm8 = vector2mask(mask.get_floats().view(np.uint32))
    return M128(blend(a.get_floats(), b.get_floats(), imm8))

def mm_ceil_pd(a: M128D) -> M128D:
    return M128D(np.ceil(a.get_doubles()))

def mm_ceil_ps(a: M128) -> M128:
    return M128(np.ceil(a.get_floats()))

def mm_ceil_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.copy()
    tmp.set_first(np.ceil(b.get_first()))
    return tmp

def mm_ceil_ss(a: M128, b: M128) -> M128:
    tmp = a.copy()
    tmp.set_first(np.ceil(b.get_first()))
    return tmp

def mm_cmpeq_epi64(a: M128I, b: M128I) -> M128I:
    return M128I(bools_to_mask(a.get_u64() == b.get_u64(), 64))

def mm_cvtepi16_epi32(a: M128I) -> M128I:
    return M128I(a.get_i16()[:4].astype(np.int32))

def mm_cvtepi16_epi64(a: M128I) -> M128I:
    return M128I(a.get_i16()[:2].astype(np.int64))

def mm_cvtepi32_epi64(a: M128I) -> M128I:
    return M128I(a.get_i32()[:2].astype(np.int64))

def mm_cvtepi8_epi16(a: M128I) -> M128I:
    return M128I(a.get_i8()[:8].astype(np.int16))

def mm_cvtepi8_epi32(a: M128I) -> M128I:
    return M128I(a.get_i8()[:4].astype(np.int32))

def mm_cvtepi8_epi64(a: M128I) -> M128I:
    return M128I(a.get_i8()[:2].astype(np.int64))

def mm_cvtepu16_epi32(a: M128I) -> M128I:
    return M128I(a.get_u16()[:4].astype(np.int32))

def mm_cvtepu16_epi64(a: M128I) -> M128I:
    return M128I(a.get_u16()[:2].astype(np.int64))

def mm_cvtepu32_epi64(a: M128I) -> M128I:
    return M128I(a.get_u32()[:2].astype(np.int64))

def mm_cvtepu8_epi16(a: M128I) -> M128I:
    return M128I(a.get_u8()[:8].astype(np.int16))

def mm_cvtepu8_epi32(a: M128I) -> M128I:
    return M128I(a.get_u8()[:4].astype(np.int32))

def mm_cvtepu8_epi64(a: M128I) -> M128I:
    return M128I(a.get_u8()[:2].astype(np.int64))

def mm_dp_pd(a: M128D, b: M128D, imm8: int) -> M128D:
    sum_ = 0.0
    control = imm8 >> 4
    tmp = []
    for i, j in zip(a.get_doubles(), b.get_doubles()):
        if control & 1:
            sum_ += i * j
        control = control >> 1
    for i in range(2):
        tmp.append(sum_ if imm8 & (2**i) else 0.0)
    return mm_setr_pd(*tmp)

def mm_dp_ps(a: M128, b: M128, imm8: int) -> M128:
    sum_ = 0.0
    control = imm8 >> 4
    tmp = []
    for i, j in zip(a.get_floats(), b.get_floats()):
        if control & 1:
            sum_ += i * j
        control = control >> 1
    for i in range(4):
        tmp.append(sum_ if imm8 & (2**i) else 0.0)
    return mm_setr_ps(*tmp)

def mm_extract_epi32(a: M128I, imm8: int) -> int:
    return a.get_i32()[imm8 & 3]

def mm_extract_epi64(a: M128I, imm8: int) -> int:
    return a.get_i64()[imm8 & 1]

def mm_extract_epi8(a: M128I, imm8: int) -> int:
    return a.get_i8()[imm8 & 0xf]

# the intel doc has the return type as a int for some reason
def mm_extract_ps(a: M128, imm8: int) -> float:
    return a.get_floats()[imm8 & 3]

def mm_floor_pd(a: M128D) -> M128D:
    return M128D(np.floor(a.get_doubles()))

def mm_floor_ps(a: M128) -> M128:
    return M128(np.floor(a.get_floats()))

def mm_floor_sd(a: M128D, b: M128D) -> M128D:
    tmp = a.copy()
    tmp.set_first(np.floor(b.get_first()))
    return tmp

def mm_floor_ss(a: M128, b: M128) -> M128:
    tmp = a.copy()
    tmp.set_first(np.floor(b.get_first()))
    return tmp

def mm_insert_epi32(a: M128I, i: int, imm8: int) -> M128I:
    tmp = a.copy()
    tmp.get_i32()[imm8 & 3] = i
    return tmp

def mm_insert_epi64(a: M128I, i: int, imm8: int) -> M128I:
    tmp = a.copy()
    a.get_i64()[imm8 & 1] = i
    return tmp

def mm_insert_epi8(a: M128I, i: int, imm8: int) -> M128I:
    tmp = a.copy()
    tmp.get_i8()[imm8 & 0xf] = i
    return tmp

def mm_insert_ps(a: M128, b: M128, imm8: int) -> M128:
    tmp = a.get_floats().copy()
    src = (imm8 >> 6) & 3
    dst = (imm8 >> 4) & 3
    tmp[dst] = b.get_floats()[src]
    for i in range(4):
        if imm8 & 1:
            tmp[i] = 0.0
    return M128(tmp)

def mm_max_epi32(a: M128I, b: M128I) -> M128I:
    return M128I(np.maximum(a.get_i32(), b.get_i32()))

def mm_max_epi8(a: M128I, b: M128I) -> M128I:
    return M128I(np.maximum(a.get_i8(), b.get_i8()))

def mm_max_epu16(a: M128I, b: M128I) -> M128I:
    return M128I(np.maximum(a.get_u16(), b.get_u16()))

def mm_max_epu32(a: M128I, b: M128I) -> M128I:
    return M128I(np.maximum(a.get_u32(), b.get_u32()))

def mm_min_epi32(a: M128I, b: M128I) -> M128I:
    return M128I(np.minimum(a.get_i32(), b.get_i32()))

def mm_min_epi8(a: M128I, b: M128I) -> M128I:
    return M128I(np.minimum(a.get_i8(), b.get_i8()))

def mm_min_epu16(a: M128I, b: M128I) -> M128I:
    return M128I(np.minimum(a.get_u16(), b.get_u16()))

def mm_min_epu32(a: M128I, b: M128I) -> M128I:
    return M128I(np.minimum(a.get_u32(), b.get_u32()))

def mm_minpos_epu16(a: M128I) -> M128I:
    dst = mm_setzero_si128()
    dst_a = dst.get_u16()
    dst_a[0] = np.min(a.get_u16())  # smallest value
    dst_a[1] = np.where(a.get_u16() == dst_a[0])[0][0]  # index of value
    return dst

def mm_mpsadbw_epu8(a: M128I, b: M128I, imm8: int) -> M128I:
    a_offset = imm8 & 4
    b_offset = 4 * (imm8 & 3)
    tmp1 = a.get_u8()
    tmp2 = b.get_u8().astype(np.int16)
    dst = np.zeros((8, ), dtype=np.uint16)
    for i in range(8):
        k = i + a_offset
        for j in range(4):
            dst[i] += abs(tmp1[j + k] - tmp2[j + b_offset])
    return M128I(dst)

def mm_mul_epi32(a: M128I, b: M128I) -> M128I:
    tmp1 = a.get_i32()[0] * b.get_i32()[0].astype(np.int64)
    tmp2 = a.get_i32()[2] * b.get_i32()[2].astype(np.int64)
    return mm_set_epi64x(tmp2, tmp1)

def mm_mullo_epi32(a: M128I, b: M128I) -> M128I:
    return M128I(a.get_i32() * b.get_i32())

def mm_packus_epi32(a: M128I, b: M128I) -> M128I:
    return M128I(saturation(np.concatenate((a.get_i32(), b.get_i32())), np.uint16))

def mm_round_pd(a: M128D, rounding: int) -> M128D:
    return M128D(round_to(a.get_doubles(), rounding))

def mm_round_ps(a: M128, rounding: int) -> M128:
    return M128(round_to(a.get_floats(), rounding))

def mm_round_sd(a: M128D, b: M128D, rounding: int) -> M128D:
    dst = a.copy()
    dst_a = dst.get_doubles()
    dst_a[0] = round_to(b.get_first(), rounding)
    return dst

def mm_round_ss(a: M128, b: M128, rounding: int) -> M128:
    dst = a.copy()
    dst_a = dst.get_floats()
    dst_a[0] = round_to(b.get_first(), rounding)
    return dst

def mm_stream_load_si128(mem_addr: (bytes, int)) -> M128I:
    return mm_load_si128(mem_addr)

def mm_test_all_ones(a: M128I) -> int:
    ones = ~np.zeros((4,), dtype=np.uint32)
    _, cf = test_zf_cf(a.get_u32(), ones)
    return cf

def mm_test_all_zeros(a: M128I, mask: M128I) -> int:
    zf, _ = test_zf_cf(a.get_u32(), mask.get_u32())
    return zf

def mm_test_mix_ones_zeros(a: M128I, mask: M128I) -> int:
    zf, cf = test_zf_cf(a.get_u32(), mask.get_u32())
    if zf == cf == 0:
        return 1
    else:
        return 0

def mm_testc_si128(a: M128I, b: M128I) -> int:
    _, cf = test_zf_cf(a.get_u32(), b.get_u32())
    return cf

def mm_testnzc_si128(a: M128I, b: M128I) -> int:
    return mm_test_mix_ones_zeros(a, b)

def mm_testz_si128(a: M128I, b: M128I) -> int:
    return mm_test_all_zeros(a, b)
