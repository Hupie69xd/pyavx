import numpy as np

from SSE2 import M128I


#### Intrinsics
def mm_cmpestra (a: M128I, la: int, b: M128I, lb: int, imm8: int) -> int :
def mm_cmpestrc (a: M128I, la: int, b: M128I, lb: int, imm8: int) -> int :
def mm_cmpestri (a: M128I, la: int, b: M128I, lb: int, imm8: int) -> int :
def mm_cmpestrm (a: M128I, la: int, b: M128I, lb: int, imm8: int) -> M128I :
def mm_cmpestro (a: M128I, la: int, b: M128I, lb: int, imm8: int) -> int :
def mm_cmpestrs (a: M128I, la: int, b: M128I, lb: int, imm8: int) -> int :
def mm_cmpestrz (a: M128I, la: int, b: M128I, lb: int, imm8: int) -> int :
def mm_cmpgt_epi64 (a: M128I, b: M128I) -> M128I :
def mm_cmpistra (a: M128I, b: M128I, imm8: int) -> int :
def mm_cmpistrc (a: M128I, b: M128I, imm8: int) -> int :
def mm_cmpistri (a: M128I, b: M128I, imm8: int) -> int :
def mm_cmpistrm (a: M128I, b: M128I, imm8: int) -> M128I :
def mm_cmpistro (a: M128I, b: M128I, imm8: int) -> int :
def mm_cmpistrs (a: M128I, b: M128I, imm8: int) -> int :
def mm_cmpistrz (a: M128I, b: M128I, imm8: int) -> int :
def mm_crc32_u16(crc: int, v: int) -> int :
def mm_crc32_u32(crc: int, v: int) -> int :
def mm_crc32_u64(crc: int, v: int) -> int :
def mm_crc32_u8 (crc: int, v: int) -> int :
